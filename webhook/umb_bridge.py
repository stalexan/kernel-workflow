"""Process bugzilla events from the UMB."""
from collections import defaultdict
from dataclasses import dataclass
from dataclasses import field
from enum import Enum
from functools import cached_property
import sys
from threading import Lock
from threading import Thread
import time
import typing

from cki_lib import logger
from gitlab.exceptions import GitlabListError
from pika.exceptions import AMQPError

from webhook import common
from webhook import defs
from webhook.description import MRDescription
from webhook.libjira import JiraField
from webhook.session import SessionRunner

if typing.TYPE_CHECKING:
    from gitlab.client import Gitlab
    from gitlab.v4.objects.merge_requests import ProjectMergeRequest

    from webhook.graphql import GitlabGraph
    from webhook.rh_metadata import Project
    from webhook.rh_metadata import Projects
    from webhook.session_events import AmqpEvent
    from webhook.session_events import GitlabMREvent
    from webhook.session_events import JiraEvent

LOGGER = logger.get_logger('cki.webhook.umb_bridge')
SEND_DELAY = 1


# A type hint to represent an iterable of of MRInfo objects
MRInfoList = typing.Iterable['MRInfo']


class KwfBridgeMsg(typing.NamedTuple):
    """Format of a defs.UMB_BRIDGE_MESSAGE_TYPE."""

    mrpath: str
    jira_key: str

    @classmethod
    def new(cls, mrpath: str = '', jira_key: str = '') -> typing.Self:
        """Construct a new KwfBridgeMsg object."""
        if not mrpath and not jira_key:
            raise ValueError('Must set at least one of mrpath or jira_key.')
        if not all(isinstance(param, str) for param in ('mrpath', 'jira_jey')):
            raise TypeError('All parameters must be strings.')
        return cls(mrpath=mrpath, jira_key=jira_key)


class BridgeMsgTarget(Enum):
    """Possible target webhooks of a KwfBridgeMsg."""

    # This would be better in rh_webhooks.yaml but here we are.

    # Value must match the name of the webhook that should process a generated
    # MessageType.UMB_BRIDGE message.

    BACKPORTER = 'backporter'
    BUGHOOK = 'bughook'
    JIRAHOOK = 'jirahook'


# This is a cut down copy of the JiraIssue object from kmt.
class JiraIssue(dict):
    """Wrapper for a Jira issue dict."""

    def __repr__(self) -> str:
        """Show yourself."""
        component = self.component or None
        cves = self.cves or None
        rep_str = f'component: {component}, fix_version: {self.fix_version}, cves: {cves}'
        return f'<{self.__class__.__name__} {self.key}, {rep_str}>'

    @property
    def component(self) -> str:
        """Return the first component of the issue."""
        components = self.fields.get('components')
        return components[0]['name'].split(' / ', 1)[0] if components else ''

    @property
    def cves(self) -> typing.List[str]:
        """Return the list of CVE IDs found in the issue labels."""
        return [label for label in self.labels if label.startswith('CVE-')]

    @property
    def fields(self) -> typing.Dict[str, typing.Any]:
        """Return the fields of the issue."""
        return self.get('fields', {})

    @property
    def fix_version(self) -> typing.Union[defs.FixVersion, None]:
        """Return the first FixVerion of the issue, or None if it is not set."""
        fix_versions = self.fields.get('fixVersions')
        return defs.FixVersion(fix_versions[0]['name']) if fix_versions else None

    @property
    def key(self) -> defs.JiraKey:
        """Return the JiraKey."""
        return defs.JiraKey(self['key'])

    @property
    def labels(self) -> typing.List[str]:
        """Return the list of labels on the issue."""
        return self.fields.get('labels', [])

    @property
    def commit_hashes(self) -> typing.Union[str, None]:
        """Return the contents of the Commit Hashes custom field."""
        return self.fields.get(JiraField.Commit_Hashes, None)


def get_session_projects(
    rh_projects: 'Projects',
    routing_keys: typing.List[str],
    sandbox_session: bool
) -> typing.List['Project']:
    """Return the list of rh_metadata.Project objects relevant to the given routing keys."""
    projects = []
    for key in routing_keys:
        if not key.startswith('gitlab.com.') or not key.endswith('.merge_request'):
            LOGGER.info("Unrecognized routing key '%s', skipping.", key)
            continue
        namespace = '/'.join(key.split('.')[2:-1])
        if not (project := rh_projects.get_project_by_namespace(namespace)):
            LOGGER.warning("Namespace '%s' not found in metadata, skipping.", namespace)
            continue
        # Only include projects that match the given session environment.
        if sandbox_session != project.sandbox:
            LOGGER.info("Skipping %s as environment does not match sandbox: %s.", project,
                        sandbox_session)
            continue
        projects.append(project)
    return projects


def get_project_mrs(
    gl_instance: 'Gitlab',
    project_namespace: str
) -> typing.List['ProjectMergeRequest']:
    """Return the list of open MRs in the given project namespace."""
    gl_project = gl_instance.projects.get(project_namespace, lazy=True)
    try:
        return gl_project.mergerequests.list(all=True, state='opened', per_page=100)
    except GitlabListError as err:
        if err.response_code == 403:
            LOGGER.error("User '%s' does not have access to %s, skipping.",
                         gl_instance.user.username, project_namespace)
        else:
            raise
    return []


def get_project_mr_infos(
    gl_instance: 'Gitlab',
    graphql: 'GitlabGraph',
    rh_project: 'Project'
) -> typing.List['MRInfo']:
    """Return a list of MRInfo objects derived from open MRs in the given rh_metadata.Project."""
    mr_infos = []
    for gl_mr in get_project_mrs(gl_instance, rh_project.id):
        if not (branch := rh_project.get_branch_by_name(gl_mr.target_branch)):
            LOGGER.warning('%s does not have a matching metadata Branch.', gl_mr.web_url)
            continue
        mr_infos.append(MRInfo.new(gl_mr.web_url, branch.fix_versions, gl_mr.description, graphql))
    return mr_infos


def get_projects_mr_infos(
    gl_instance: 'Gitlab',
    graphql: 'GitlabGraph',
    rh_projects: typing.Iterable['Project']
) -> typing.List['MRInfo']:
    """Return a list of MRInfo objects derived from the given Projects."""
    return [mr for p in rh_projects for mr in get_project_mr_infos(gl_instance, graphql, p)]


def _set_up_send_queue(send_queue, send_message):
    """Set up the send_queue."""
    # Before we do anything check the sender thread is running.
    send_queue.check_status()

    if send_queue.send_function is None and not send_queue.fake_queue:
        LOGGER.info('Setting send_queue.send_message to: %s', send_message)
        send_queue.send_function = send_message


def process_amqp_event(
    _: dict,
    session: SessionRunner,
    event: 'AmqpEvent',
    mr_info_cache: 'MRInfoCache',
    send_queue: 'SendQueue',
    **__: typing.Any
) -> None:
    """Process UMB bugzilla events from Amqp."""
    _set_up_send_queue(send_queue, session.queue.send_message)
    if not (matching_refs := mr_info_cache.match(bugs=[event.bug_id])):
        LOGGER.info('Ignoring event for unknown bug %d.', event.bug_id)
        return
    LOGGER.info('Event for bug %d by %s relevant to these MRs: %s',
                event.bug_id, event.user, matching_refs)
    for mr_ref in matching_refs:
        send_queue.add_bug(mr_ref)


def process_jira_event(
    _: typing.Dict,
    session: SessionRunner,
    event: 'JiraEvent',
    mr_info_cache: 'MRInfoCache',
    send_queue: 'SendQueue',
    **__: typing.Any
) -> None:
    """Process Jira webhook events."""
    _set_up_send_queue(send_queue, session.queue.send_message)
    LOGGER.info('Processing jira event for %s triggered by %s', event.key, event.user)
    jira_issue = JiraIssue(event.body['issue'])
    if not jira_issue.component.startswith('kernel'):
        LOGGER.info('Ignoring event for non-kernel issue.')
        return
    LOGGER.info('Issue CVE labels: %s', jira_issue.cves)
    if (matching_mrs := mr_info_cache.match(jiras=[jira_issue.key],
                                            cves=jira_issue.cves,
                                            fix_version=jira_issue.fix_version)):
        LOGGER.info('Event matches %s', matching_mrs)
        for mr_ref in matching_mrs:
            send_queue.add_jira(mr_ref)
    elif jira_issue.commit_hashes:
        LOGGER.info('No known MRs in issue and commit hashes populated, pass to backporter.')
        send_queue.add_backporter(jira_issue.key)
    else:
        LOGGER.info('Ignoring event not relevant to any known MRs.')


def process_gitlab_mr(
    _: dict,
    session: SessionRunner,
    event: 'GitlabMREvent',
    mr_info_cache: 'MRInfoCache',
    **__: typing.Any
) -> None:
    """Process gitlab MR hook events."""
    # This should also pass through events showing a target branch change but at the moment
    # there is no simple way to know this.
    # https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128747
    if event.closing or event.mr_inactive:
        mr_info_cache.remove(event.mr_url)
        return
    # If an MR is opened against an unexpected target branch then we need to ignore it.
    if not event.rh_branch:
        LOGGER.info('Event is not associated with a recognized branch, ignoring.')
        return
    mr_info = MRInfo.new(
        url=event.mr_url,
        fix_versions=event.rh_branch.fix_versions,
        description=event.merge_request.get('description', ''),
        graphql=session.graphql
    )
    mr_info_cache.add(mr_info)


@dataclass(frozen=True)
class MRInfo:
    """Tiny object to hold MR details."""

    url: defs.GitlabURL
    fix_versions: typing.FrozenSet[defs.FixVersion] = frozenset()
    bugs: typing.FrozenSet[int] = frozenset()
    cves: typing.FrozenSet[str] = frozenset()
    jiras: typing.FrozenSet[defs.JiraKey] = frozenset()

    def __repr__(self) -> str:
        """Say hi."""
        fvs_str = ', '.join(self.fix_versions) if self.fix_versions else 'None'
        return f'<{self.__class__.__name__} {self.gl_reference} (fix_versions: {fvs_str})>'

    @classmethod
    def new(cls, url: str, fix_versions: typing.Iterable[str], description: str,
            graphql: 'GitlabGraph') -> 'MRInfo':
        """Construct a new MRInfo object."""
        url = defs.GitlabURL(url)
        mr_description = MRDescription(description, url.namespace, graphql)
        bugs = mr_description.bugzilla | mr_description.depends_bzs
        cves = mr_description.cve
        jiras = mr_description.jira_tags | mr_description.depends_jiras
        return cls(url, frozenset(defs.FixVersion(fv) for fv in fix_versions),
                   frozenset(bugs), frozenset(cves), frozenset(jiras))

    @cached_property
    def gl_reference(self) -> str:
        """Return a reference to the MR in the Gitlab format 'namespace!ID'."""
        if self.url.type != 'merge_requests':
            raise RuntimeError(f"Only MRs are supported, not '{self.url.type}'!")
        return f'{self.url.namespace}!{self.url.id}'


MRInfoDict = typing.Dict[defs.GitlabURL, MRInfo]
BugDict = typing.DefaultDict[int, typing.Set[MRInfo]]
CveDict = typing.DefaultDict[str, typing.Set[MRInfo]]
JiraDict = typing.DefaultDict[defs.JiraKey, typing.Set[MRInfo]]


@dataclass
class MRInfoCache:
    """Maintains a mapping of bugs/cves/jiras to MRs."""

    # mr_infos: a dict with MR URLs keys and MRInfo instances as the values.
    #
    # rebuild_caches() parses all the current mr_infos and populates new bugs/cves/jiras dicts.
    mr_infos: MRInfoDict = field(default_factory=dict)
    bugs: BugDict = field(init=False, default_factory=lambda: defaultdict(set))
    cves: CveDict = field(init=False, default_factory=lambda: defaultdict(set))
    jiras: JiraDict = field(init=False, default_factory=lambda: defaultdict(set))

    def __post_init__(self) -> None:
        """Populate the caches."""
        self.rebuild_caches()
        LOGGER.info('Created %s', self)

    def __repr__(self) -> str:
        """Say it out loud."""
        repr_str = f'Bugs: {len(self.bugs)}, CVEs: {len(self.cves)}, Jiras: {len(self.jiras)}'
        return f'<{self.__class__.__name__} MRs: {len(self.mr_infos)}, {repr_str}'

    @staticmethod
    def _build_caches(
        mr_infos: MRInfoDict
    ) -> typing.Tuple[typing.Dict[int, str], typing.Dict[str, str], typing.Dict[defs.JiraKey, str]]:
        """Build the caches with data from the given mr_infos."""
        bugs: BugDict = defaultdict(set)
        cves: CveDict = defaultdict(set)
        jiras: JiraDict = defaultdict(set)
        for mr_info in mr_infos.values():
            for bug in mr_info.bugs:
                bugs[bug].add(mr_info)
            for cve in mr_info.cves:
                cves[cve].add(mr_info)
            for jira in mr_info.jiras:
                jiras[jira].add(mr_info)
        return bugs, cves, jiras

    def rebuild_caches(self) -> None:
        """Rebuild the caches."""
        LOGGER.debug('Rebuilding cache...')
        self.bugs, self.cves, self.jiras = self._build_caches(self.mr_infos)

    def add(self, mr_info: MRInfo) -> None:
        """Add an MRInfo and rebuild the bugs/cves/jiras/ cache."""
        LOGGER.info('Adding %s to cache.', mr_info)
        self.mr_infos[mr_info.url] = mr_info
        self.rebuild_caches()

    def remove(self, mr_url: str) -> None:
        """Remove the given URL's entries from the cache."""
        if not (mr_info := self.mr_infos.pop(mr_url, None)):
            LOGGER.warning('%s not found in cache, nothing to remove.', mr_url)
            return
        LOGGER.info('Removed %s', mr_info)
        self.rebuild_caches()

    def match(
        self,
        bugs: typing.Optional[typing.Iterable[int]] = None,
        cves: typing.Optional[typing.Iterable[str]] = None,
        jiras: typing.Optional[typing.Iterable[defs.JiraKey]] = None,
        fix_version: typing.Optional[defs.FixVersion] = None
    ) -> typing.Set[str]:
        """Return the list of MR references from the caches which match the input."""
        mrs: typing.Set[MRInfo] = set()
        mrs.update(
            mr_info for search_key in bugs or [] for
            mr_info in self.bugs.get(int(search_key), set())
        )
        mrs.update(
            mr_info for search_key in jiras or [] for mr_info in self.jiras.get(search_key, set()))
        for cve in cves or []:
            # If given a fix_version then only include the mr_ref if it has a matching fix_version,
            # otherwise just include all of them :shrug:.
            mrs.update(
                mr_info for mr_info in self.cves.get(cve, set()) if
                not fix_version or
                fix_version in mr_info.fix_versions
            )
        return {mr_info.gl_reference for mr_info in mrs}

    @classmethod
    def new(cls, session: 'SessionRunner') -> 'MRInfoCache':
        """Return a new MRInfoCache loaded with data from the session's relevant projects."""
        projects = get_session_projects(session.rh_projects,
                                        session.args.rabbitmq_routing_key.split(),
                                        bool(not session.is_production))
        if not projects:
            raise RuntimeError('No projects found, nothing to do.')
        LOGGER.info('Loading cache data from these projects: %s', projects)
        mr_infos = get_projects_mr_infos(session.gl_instance, session.graphql, projects)
        return cls({mr_info.url: mr_info for mr_info in mr_infos})


MSG_HANDLERS = {defs.GitlabObjectKind.MERGE_REQUEST: process_gitlab_mr,
                defs.MessageType.AMQP: process_amqp_event,
                defs.MessageType.JIRA: process_jira_event
                }


class SendQueue:
    """Manage queue of MRs to send to the webhooks hook as a separate thread."""

    def __init__(self, sender_exchange, sender_route, environment, fake_queue=False):
        """Set up send params and spawn thread."""
        self.data = {}
        self.logger = logger.get_logger('cki.webhook.umb_bridge.SendQueue')
        self.fake_queue = fake_queue

        # Must be set to MessageQueue.send_message() method before using the queue.
        self.send_function = None

        # Params passed by self._send_message to send_function, excluding data.
        self.send_params = {'queue_name': sender_route,
                            'exchange': sender_exchange,
                            'headers': {'message-type': defs.UMB_BRIDGE_MESSAGE_TYPE,
                                        'event_source_environment': environment
                                        }
                            }

        self._lock = Lock()
        self._thread = Thread(target=self._sender_thread, daemon=True)

    def check_send_function(self):
        """Barf if send_function has not been set."""
        if self.fake_queue:
            return True
        if self.send_function is None:
            self.logger.warning('send_function is not set!')
            return False
        return True

    def add(self, msg_target: BridgeMsgTarget, msg_body: KwfBridgeMsg):
        """Add or update an entry in the queue."""
        if not self.check_send_function():
            return
        with self._lock:
            key = (msg_target, msg_body)
            self.logger.info('Adding %s for %s.', key[1], key[0].value)
            # Remove it so we can depend on insertion order to test age.
            if key in self.data:
                del self.data[key]
            self.data[key] = time.time()

    def add_backporter(self, jira_key: str) -> None:
        """Add or update a backporter entry in the queue."""
        body = KwfBridgeMsg.new(jira_key=jira_key)
        self.add(BridgeMsgTarget.BACKPORTER, body)

    def add_bug(self, mrpath: str) -> None:
        """Add or update a bughook entry in the queue."""
        body = KwfBridgeMsg.new(mrpath=mrpath)
        self.add(BridgeMsgTarget.BUGHOOK, body)

    def add_jira(self, mrpath: str) -> None:
        """Add or update a jirahook entry in the queue."""
        body = KwfBridgeMsg.new(mrpath=mrpath)
        self.add(BridgeMsgTarget.JIRAHOOK, body)

    def _get(self):
        """Return the oldest item at least SEND_DELAY old."""
        # Caller must hold self.lock!
        if not self.data:
            self.logger.debug('Empty.')
            return None

        # Is the first (oldest) item at least SEND_DELAY old?
        key = next(iter(self.data))
        if self.data[key] > time.time() - SEND_DELAY:
            self.logger.debug('Nothing older than SEND_DELAY (%d) in data.', SEND_DELAY)
            return None
        return key

    def _send_message(self, key: typing.Tuple[BridgeMsgTarget, KwfBridgeMsg]) -> None:
        """Send a message to the queue."""
        msg_target, msg_body = key
        body_dict = msg_body._asdict()
        try:
            # pylint: disable=not-callable
            params = self.send_params | {'data': body_dict}
            params['headers']['event_target_webhook'] = msg_target.value
            if not self.fake_queue:
                self.send_function(**params)
            self.logger.info('Sent %s for %s.', body_dict, msg_target.value)
        except AMQPError as err:
            self.logger.error('Error sending message: %s', err)
        del self.data[key]

    def send_all(self):
        """Send all outstanding messages."""
        with self._lock:
            while (key := self._get()) is not None:
                if not self.check_send_function():
                    return
                self._send_message(key)
            self.logger.debug('Nothing to send.')

    def _sender_thread(self):
        """Process send queue."""
        while True:
            time.sleep(10)
            self.send_all()

    def check_status(self):
        """Confirm the sender thread is running, otherwise die."""
        # If the sender thread is not running try to send any messages in the
        # queue and then die.
        if not self._thread.is_alive():
            self.logger.error('Sender thread not running as expected.')
            with self._lock:
                for key in self.data.copy():
                    self._send_message(key)
                sys.exit(1)
        return True

    def start(self):
        """Start the sender thread."""
        self.logger.debug('Starting sender thread.')
        self._thread.start()
        self.check_status()


def main(args):
    """Run main loop."""
    parser_prefix = 'UMB_BRIDGE'
    parser = common.get_arg_parser(parser_prefix)
    parser.add_argument('--rabbitmq-sender-exchange',
                        **common.get_argparse_environ_opts(f'{parser_prefix}_SENDER_EXCHANGE'),
                        help='RabbitMQ Exchange for sending messages.')
    parser.add_argument('--rabbitmq-sender-route',
                        **common.get_argparse_environ_opts(f'{parser_prefix}_SENDER_ROUTE'),
                        help='RabbitMQ Routing Key for sending messages.')
    parser.add_argument('--no-send-queue', action='store_true',
                        help='Run without a SendQueue for debugging purposes.')
    args = parser.parse_args(args)

    session = SessionRunner.new('umb_bridge', args=args, handlers=MSG_HANDLERS)

    # Setting --no-send-queue means umb_bridge will not generate any events so it should never
    # be used in production.
    if args.no_send_queue and session.is_production:
        raise RuntimeError('--no-send-queue should not be used in production!')

    # Initiate and start the send_queue handler.
    send_queue = SendQueue(args.rabbitmq_sender_exchange, args.rabbitmq_sender_route,
                           environment=session.environment, fake_queue=args.no_send_queue)
    send_queue.start()

    # Populate GL bug/cve/jira mapping and start consuming messages.
    mr_info_cache = MRInfoCache.new(session)
    session.run(mr_info_cache=mr_info_cache, send_queue=send_queue)


if __name__ == "__main__":
    main(sys.argv[1:])
