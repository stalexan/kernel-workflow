#!/usr/bin/env python
"""Try to backport upstream commit referenced in provided Jira issue."""

import os
import subprocess
import sys
import typing

from cki_lib import logger
from git.exc import GitCommandError

from webhook import common
from webhook import defs
from webhook import kgit
from webhook.libbackport import Backport
from webhook.libjira import JiraField
from webhook.libjira import get_linked_mrs
from webhook.libjira import update_issue_field
from webhook.rhissue import make_rhissues
from webhook.session import SessionRunner

if typing.TYPE_CHECKING:
    from git import Remote
    from gitlab.v4.objects import Project

    from webhook.rhissue import RHIssue
    from webhook.session import BaseSession
    from webhook.session_events import UmbBridgeEvent

LOGGER = logger.get_logger('webhook.utils.backporter')
MAX_PATCHES_FULL_LOG = 3
KWF_BACKPORT_SUCCESS_LABEL = 'kwf-backport-success'
KWF_BACKPORT_FAIL_LABEL = 'kwf-backport-fail'


def try_to_backport(session: 'BaseSession', backport: 'Backport') -> bool:
    """Attempt to backport the sha(s) to the given project/target_branch."""
    if not session.args.testing:
        LOGGER.debug('Fetching from git remotes to ensure we have the latest data needed')
        kgit.fetch_all(session.args.rhkernel_src)

    rhmetadata = f'JIRA: {backport.rhissue.jira_link}'
    for cve in backport.rhissue.ji_cves:
        rhmetadata += f'  \nCVE: {cve}'

    backport.description += f'{rhmetadata}\n\n'
    patch_count = len(backport.shas)

    # This initializes the worktree and branch
    repo = backport.repo

    for sha in backport.shas:
        try:
            kgit.cherry_pick(backport.worktree_dir, sha)
        except subprocess.CalledProcessError:
            kgit.worktree_remove(session.args.rhkernel_src, backport.worktree_dir)
            try:
                kgit.branch_delete(session.args.rhkernel_src, backport.bp_branch_save)
            except subprocess.CalledProcessError:
                pass
            kgit.branch_delete(session.args.rhkernel_src, backport.bp_branch)
            LOGGER.info("Unable to auto-backport commits for %s (failed on %s)",
                        backport.rhissue.id, sha)
            return False

        signoff = (f"Signed-off-by: {repo.head.commit.committer.name} "
                   f"<{repo.head.commit.committer.email}>")
        title = repo.head.commit.summary
        if patch_count > MAX_PATCHES_FULL_LOG:
            backport.description += f" * {sha} {title}  \n"
        else:
            # Patch commit message, less our signoff
            patch_desc = repo.head.commit.message.replace(f"{signoff}\n", "")
            backport.description += f'```\n{patch_desc}```\n'
        glog = repo.git.log(f'{sha}^..{sha}')
        new_msg = f'{title}\n\n{rhmetadata}\n\n{glog}'
        repo.git.commit('--reset-author', '-s', '--amend', '-m', new_msg)

    backport.description += f"\n{signoff}"

    LOGGER.info("Successfully backported commits %s for %s", backport.shas, backport.rhissue.id)
    return True


def trim_title_for_mr(title: str) -> str:
    """Clean up the Jira title for use as MR title."""
    trimmed = title.strip('TRIAGE').strip()
    if ':' in trimmed:
        parts = trimmed.split(':')
        parts[0] = parts[0].strip('kernel').strip()
        trimmed = ':'.join(parts)
    if '[' in trimmed:
        parts = trimmed.split('[')
        trimmed = ''.join(parts[:-1]).strip()
    return trimmed


def git_branch_exists(backport: 'Backport', bot_remote: 'Remote') -> bool:
    """Check if a git branch exists in the given remote."""
    kgit.prune_remote(backport.repo.working_tree_dir, bot_remote.name)
    remote_ref_name = f'{bot_remote.name}/{backport.repo.active_branch.name}'
    return any(ref.name == remote_ref_name for ref in bot_remote.refs)


def mr_for_branch_exists(gl_project: 'Project', bp_branch: str, target_branch: str) -> bool:
    """Check if an MR created from this branch already exists."""
    mr_list = gl_project.mergerequests.list(all=True, state='opened', target_branch=target_branch)
    for mr in mr_list:
        LOGGER.info("bp_branch: %s, source_branch: %s", bp_branch, mr.source_branch)
    return any(bp_branch == mr.source_branch for mr in mr_list)


def add_info_to_jira_issue(session: 'BaseSession', backport: 'Backport',
                           success: bool, mr_url: str) -> None:
    """Add backport success or failure label to Jira issue."""
    jira_assignee = backport.rhissue.ji.fields.assignee.name
    jira_labels = backport.rhissue.ji.fields.labels
    label = KWF_BACKPORT_SUCCESS_LABEL
    if success:
        comment = ("KWF backport automation successfully created a merge request for "
                   f"this Jira issue at {mr_url}. This issue still needs Dev Target "
                   f"Milestone set by [~{jira_assignee}], and developer review done in "
                   "the merge request to progress.")
    else:
        label = KWF_BACKPORT_FAIL_LABEL
        comment = ("KWF backport automation was unable to create a merge request for "
                   f"this Jira issue. Manual intervention from [~{jira_assignee}] required.")
    jira_labels.append(label)
    LOGGER.info("Adding label %s to Jira issue %s and leaving comment: %s",
                label, backport.rhissue.id, comment)
    if session.is_production:
        backport.rhissue.ji.update(fields={"labels": jira_labels})
        session.jira.add_comment(backport.rhissue.id, comment)


def submit_merge_request(session: 'BaseSession', backport: 'Backport') -> None:
    """Submit a merge request using the successful auto-backport."""
    LOGGER.debug("Creating backport for %s from %s", backport.rhissue.id, backport.worktree_dir)
    mr_title = trim_title_for_mr(backport.rhissue.ji.fields.summary)

    gl_project = session.get_gl_project(backport.rhissue.ji_project.namespace)
    fork_ns = f'{defs.BOT_FORK_NAMESPACE_BASE}/{gl_project.name}'
    fork_project = session.gl_instance.projects.get(fork_ns)
    target_branch = backport.rhissue.ji_branch.name
    if session.args.testing:
        LOGGER.info("Not pushing in testing mode. Backport details: %s\n"
                    "Source Project: %s\nTarget Project: %s\n"
                    "MR Details:\n%s\n\n%s\n\nHEAD Commit details:\n%s",
                    backport, fork_project.path_with_namespace, gl_project.path_with_namespace,
                    mr_title, backport.description, backport.repo.head.commit.message)
        return

    try:
        bot_remote = backport.repo.remote(f'bot-{backport.rhissue.ji_project.name}')
    except ValueError:
        LOGGER.error("Backporter bot remote %s not found in git checkout",
                     f'bot-{backport.rhissue.ji_project.name}')
        return

    if not git_branch_exists(backport, bot_remote):
        try:
            bot_remote.push().raise_if_error()
        except GitCommandError:
            LOGGER.error("Branch push failed for unknown reason, aborting operation")
            return
    else:
        LOGGER.info("Existing backport branch found in bot remote")
        if kgit.branches_differ(backport.worktree_dir, backport.bp_branch, backport.bp_branch_save):
            bot_remote.push(force=True)
            LOGGER.info("Updated existing branch for %s", backport.rhissue.id)
        else:
            LOGGER.info("Latest backport attempt does not differ from prior one")
        kgit.branch_delete(session.args.rhkernel_src, backport.bp_branch_save)
        return

    if mr_for_branch_exists(gl_project, backport.bp_branch, target_branch):
        LOGGER.info("Existing open MR found, not creating a new one")
        return

    mr_data = {
        'source_branch': backport.bp_branch,
        'target_branch': target_branch,
        'title': mr_title,
        'description': backport.description,
        'target_project_id': gl_project.id
    }
    new_mr = fork_project.mergerequests.create(mr_data)

    add_info_to_jira_issue(session, backport, success=True, mr_url=new_mr.web_url)
    LOGGER.info("New MR: %s", new_mr.web_url)


def should_try_backport(session: 'BaseSession', rhissue: 'RHIssue') -> bool:
    """Evaluate if we should try backporting for this Jira issue or not."""
    should_try = True

    if not rhissue.ji_project:
        LOGGER.info("No project found for environment, aborting.")
        should_try = False

    elif KWF_BACKPORT_FAIL_LABEL in rhissue.ji.fields.labels and not session.args.testing:
        LOGGER.info("Prior backport attempts already failed, aborting.")
        should_try = False

    elif KWF_BACKPORT_SUCCESS_LABEL in rhissue.ji.fields.labels and not session.args.testing:
        LOGGER.info("Prior backport attempt was successful, no need to try again.")
        should_try = False

    elif not rhissue.ji_commit_hashes:
        LOGGER.info("Aborting due to %s having no Commit Hashes specified", rhissue.id)
        should_try = False

    elif get_linked_mrs(rhissue.ji, jiracon=session.jira) and not session.args.testing:
        LOGGER.info("Aborting due to existing MR in %s", rhissue.id)
        should_try = False

    if not should_try:
        return should_try

    if not rhissue.ji_fix_version and rhissue.ji_affects_versions:
        LOGGER.info("Setting Fix Version to Affects Version %s in %s",
                    rhissue.ji_affects_versions[0], rhissue.id)
        update_issue_field(rhissue.ji, JiraField.fixVersions, rhissue.ji_affects_versions[0])

    if not rhissue.ji_fix_version:
        LOGGER.info("Aborting due to no Fix Version set in %s", rhissue.id)
        should_try = False

    return should_try


def process_event(
    _: dict,
    session: SessionRunner,
    event: 'UmbBridgeEvent',
    **__: typing.Any
) -> None:
    """Process an event."""
    rhissue = make_rhissues([event.jira_key], None,
                            jira=session.jira, projects=session.rh_projects)[0]
    LOGGER.debug("Got issue %s (%s), commit hashes: %s",
                 rhissue.id, rhissue.ji.fields.summary, rhissue.ji_commit_hashes)

    if not should_try_backport(session, rhissue):
        return

    if not session.args.testing:
        kgit.setup_git_user(session.args.rhkernel_src, session.args.name, session.args.email)

    backport = Backport(rhissue, session.args.rhkernel_src)

    LOGGER.debug("%s", backport)

    if try_to_backport(session, backport):
        submit_merge_request(session, backport)
    else:
        add_info_to_jira_issue(session, backport, success=False, mr_url='')


HANDLERS = {
    defs.MessageType.UMB_BRIDGE: process_event
}


def main(args) -> None:
    """Find open MRs and check for merge conflicts."""
    parser = common.get_arg_parser('BACKPORTER')
    parser.add_argument('-r', '--rhkernel-src', default=os.environ.get('RHKERNEL_SRC', ''),
                        help='Directory containing RH kernel projects git tree')
    parser.add_argument('-T', '--testing', action='store_true', default=False,
                        help="Run in testing mode, omit certain checks")
    parser.add_argument('-n', '--name', default=os.environ.get('GIT_COMMITTER_NAME', ''),
                        help="Name to use for git commits and signoffs")
    parser.add_argument('-e', '--email', default=os.environ.get('GIT_COMMITTER_EMAIL', ''),
                        help="Email address to use for git commits and signoffs")
    pargs = parser.parse_args(args)

    if not pargs.rhkernel_src:
        LOGGER.warning("No path to RH Kernel source git found, aborting!")
        return

    if not pargs.jira:
        LOGGER.warning("No jira issue specified (-j/--jira), aborting!")
        return

    session = SessionRunner.new('backporter', args=pargs, handlers=HANDLERS)
    session.run()


if __name__ == '__main__':
    main(sys.argv[1:])
