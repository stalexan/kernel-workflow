"""Confirm all owners.yaml users are members of the necessary RH groups & projects."""
import argparse
from dataclasses import dataclass
from enum import Enum
from os import environ
import sys
import typing

from cki_lib.footer import Footer
from cki_lib.logger import get_logger
from cki_lib.metrics import prometheus_init
from cki_lib.misc import sentry_init
import prometheus_client as prometheus
import sentry_sdk

from webhook.common import get_owners_parser
from webhook.session import BaseSession

if typing.TYPE_CHECKING:
    from webhook.graphql import GitlabGraph

# Used by argparse print_help().
DESCRIPTION = 'Validates owners.yml usernames. At least one group or project name must be provided.'

# Home of owners.yaml
DOCS_PROJECT = environ.get('GL_DOCS_PROJECT', 'redhat/centos-stream/src/kernel/documentation')

METRIC_KWF_MISSING_USERS = prometheus.Gauge(
    'kwf_missing_users',
    'Number of owners.yaml users missing from the given group or project.',
    ['group_or_project']
)

METRIC_KWF_ARK_MISSING_FROM_REDHAT = prometheus.Gauge(
    'kwf_ark_missing_from_redhat',
    'Number of kernel-ark project members missing from the redhat group.'
)

# Title of an issue created by the --create-issues option.
ISSUE_TITLE = 'Found owners.yaml username without expected group & project membership: '

# Body of an issue created by the --create-issues option.
ISSUE_TEXT = """
@%s is listed in the owners.yaml file but was not found to be a member of all the expected
Gitlab groups & projects. This means the user will not be able to properly participate in
reviews, may not receive the expected notifications, and will not be able to Approve MRs.

All users listed in the owners.yaml file must be a member of the @redhat group and a member
of the cki-project/kernel-ark> project.

This user is missing from:
%s

If this user is a Red Hat employee that is expected to participate then please verify they
have the proper permissions. Membership in the @redhat group should be automatic if the user
has connected their account to SSO via [this link](https://red.ht/GitLabSSO). Membership in
the cki-project/kernel-ark> project is manual; please contact a maintainer for assistance.

If this user is not a Red Hat employee or is no longer expected to participate then please
open an MR in this project that references this Issue to update the owners.yaml file.
"""

LOGGER = get_logger('cki.webhook.owners_validator')


def get_parser():
    """Return the parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    yaml_help = 'path of owners.yaml file to validate'
    if default_yaml := environ.get('OWNERS_YAML'):
        parser.add_argument('-o', '--owners-yaml', help=yaml_help, default=default_yaml)
    else:
        parser.add_argument('-o', '--owners-yaml', help=yaml_help, required=True)
    parser.add_argument('-g', '--groups', default=environ.get('GL_GROUPS', '').split(),
                        help='gitlab groups to validate membership in', nargs='+')
    parser.add_argument('-p', '--projects', default=environ.get('GL_PROJECTS', '').split(),
                        help='gitlab groups to validate membership in', nargs='+')
    parser.add_argument('-c', '--create-issues', action='store_true',
                        help=f'create Issues reporting missing users on the {DOCS_PROJECT} project')
    parser.add_argument('-l', '--label-issues', nargs='+', default=[],
                        help=('a list of labels to add to any Issues created when --create-issues'
                              ' is set'))
    parser.add_argument('-m', '--metrics', action='store_true', help='enables prometheus metrics')
    parser.add_argument('--sentry-ca-certs', default=environ.get('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    return parser


# The type for the GLNamespace dataclass.
NS_TYPE = Enum('Group Type', ['group', 'project'])


@dataclass(repr=False)
class GLNamespace:
    """A group or project depending on type."""

    name: str
    type: NS_TYPE
    members: set
    missing: set

    def __post_init__(self):
        """Log yourself."""
        LOGGER.info('Created %s', self)
        LOGGER.info(self.report)

    def __repr__(self):
        """Represent yourself."""
        repr_str = f"'{self.name}', members: {len(self.members)}, missing: {len(self.missing)}"
        return f'<GLNamespace {self.type.name}: {repr_str}>'

    @property
    def markdown(self):
        """Return a markdown formatted string with the type and name."""
        return f'group @{self.name}' if self.type is NS_TYPE.group else f'project {self.name}>'

    @property
    def report(self):
        # pylint: disable=consider-using-f-string
        """Generate a report string."""
        if self.missing:
            return "%s owners.yaml user%s missing from '%s': %s" % \
                (len(self.missing), 's' if len(self.missing) > 1 else '', self.name, self.missing)
        return f"All owners.yaml usernames found in {self.type.name} '{self.name}' 👍"


def build_namespace_dict(graphql, groups, projects, owners_users):
    """Return a dict of GLNamespace objects from the input groups/projects list and owners set."""
    members = {}
    for namespace in groups + projects:
        # Blow up if the group or project has zero members because that would be weird.
        ns_type = NS_TYPE.group if namespace in groups else NS_TYPE.project
        if not (raw_users := graphql.get_all_members(namespace, ns_type.name)):
            raise ValueError(f'No users found in {namespace}!')
        users = set(raw_users.keys())
        members[namespace] = GLNamespace(namespace, ns_type, users, owners_users.difference(users))
    return members


def usernames_in_owners(owners_yaml_path):
    """Return the set of all GL usernames in the given owners.yaml using an owners_parser."""
    owners = get_owners_parser(owners_yaml_path)
    if not (owners_usernames := {user['gluser'] for s in owners.subsystems for
                                 user in s.maintainers + s.reviewers if user['gluser']}):
        raise ValueError('⚠⚠⚠No usernames found in the owners parser ⚠⚠⚠')
    return owners_usernames


def check_ark_members(ns_members, do_metric):
    """Check if Ark project members are also redhat group members, if we have the data to do so."""
    # It is expected that some users in kernel-ark (like bots) are not redhat group members.
    redhat = ns_members.get('redhat')
    ark = ns_members.get('cki-project/kernel-ark')
    if not redhat or not ark:
        LOGGER.debug('check_ark_members: data needed is not present, skipping check.')
        return
    if ark_users_not_in_rh := ark.members.difference(redhat.members):
        LOGGER.info("Found Ark project members who are not 'redhat' group members: %s",
                    ark_users_not_in_rh)
    else:
        LOGGER.info("All kernel-ark project members are 'redhat' group members.")
    if do_metric:
        METRIC_KWF_ARK_MISSING_FROM_REDHAT.set(len(ark_users_not_in_rh))


def create_issues(
    graphql: 'GitlabGraph',
    target_project: str,
    ns_members: dict[str, GLNamespace],
    labels: typing.Optional[list[str]] = None,
    footer: str = ''
) -> None:
    """Create documentation project issues for the missing users."""
    # Get all the open Issues and create the dict of usernames that already have Issues.
    open_issues = graphql.get_all_issues(target_project)
    users_with_issues = {issue['title'].removeprefix(ISSUE_TITLE): issue['webUrl'] for
                         issue in open_issues if issue['title'].startswith(ISSUE_TITLE)}
    LOGGER.info('Found %s open Issues in the %s project', len(open_issues), target_project)
    LOGGER.info('%s users have existing Issues', len(users_with_issues))

    # Generate the list of owners.yaml usernames that are not members of any namespace and see if
    # they are even known GL usernames.
    if missing_from_all := set.intersection(*[gl_ns.missing for gl_ns in ns_members.values()]):
        LOGGER.info('Found usernames missing from all groups/projects: %s', missing_from_all)
    unknown_usernames = all_usernames_valid(graphql, missing_from_all)

    # Create an Issue for each missing user.
    for username in {user for gl_ns in ns_members.values() for user in gl_ns.missing}:
        if username in users_with_issues:
            LOGGER.info('Found existing Issue for %s: %s', username, users_with_issues[username])
            continue
        # Create a markdown list of problems.
        markdown_list_str = ''
        if username in unknown_usernames:
            markdown_list_str += f'- @{username} does not appear to be a user known to Gitlab\n'
        # Add groups/projects the username is missing from.
        for gl_ns in [gl_ns for gl_ns in ns_members.values() if username in gl_ns.missing]:
            markdown_list_str += f'- {gl_ns.markdown}\n'
        # Set any labels.
        extra_input = {'labels': labels} if labels else None
        issue_text = ISSUE_TEXT % (username, markdown_list_str) + footer
        new_issue = graphql.create_project_issue(target_project, ISSUE_TITLE + username,
                                                 issue_text, extra_input=extra_input)
        LOGGER.info('Created new Issue for %s: %s', username, new_issue['webUrl'])
        LOGGER.debug('Title: %s\n%s', ISSUE_TITLE + username, issue_text)


def all_usernames_valid(graphql, usernames):
    """Return the list of input usernames which are unknown to GL."""
    LOGGER.info('Looking up %s user%s: %s', len(usernames), 's' if len(usernames) > 1 else '',
                usernames)
    if unknown_usernames := [username for username in usernames if not graphql.get_user(username)]:
        LOGGER.warning('The following usernames do not appear to be known to Gitlab: %s',
                       unknown_usernames)
    else:
        LOGGER.info('All provided usernames were found on Gitlab 👍.')
    return unknown_usernames


def main(session: BaseSession, args: argparse.Namespace) -> None:
    """Run it."""
    # Populate the owners_usernames set from the owners file.
    owners_usernames = usernames_in_owners(args.owners_yaml)
    LOGGER.info('Found %s usernames in %s', len(owners_usernames), args.owners_yaml)

    # If no groups/projects were given then look up all the owners usernames to confirm they exist.
    if not args.groups and not args.projects:
        retval = len(all_usernames_valid(session.graphql, owners_usernames))
        sys.exit(retval)

    # Populate the membership dict.
    ns_members = build_namespace_dict(session.graphql, args.groups, args.projects, owners_usernames)

    # If we have em, log kernel-ark memebers who are not in the redhat group.
    check_ark_members(ns_members, args.metrics)

    # Populate the set of users missing from somewhere; if none then we have nothing else to do.
    if not any(gl_ns for gl_ns in ns_members.values() if gl_ns.missing):
        LOGGER.info('All owners.yaml usernames are members of the given groups/projects 👍')
        return

    # Set some prometheus metrics.
    if args.metrics and 'redhat' in ns_members:
        METRIC_KWF_MISSING_USERS.labels('redhat').set(len(ns_members['redhat'].missing))
    if args.metrics and 'cki-project/kernel-ark' in ns_members:
        METRIC_KWF_MISSING_USERS.labels('kernel-ark').set(
            len(ns_members['cki-project/kernel-ark'].missing)
        )

    # Create Issues on the documentation project to notify about missing users.
    footer = Footer(session.rh_projects.webhooks['owners_validator']).gitlab_footer()
    if args.create_issues:
        create_issues(session.graphql, DOCS_PROJECT, ns_members, args.label_issues, footer=footer)


if __name__ == '__main__':
    parse_args = get_parser().parse_args()
    prometheus_init()
    sentry_init(sentry_sdk, ca_certs=parse_args.sentry_ca_certs)
    main(BaseSession.new(), parse_args)
