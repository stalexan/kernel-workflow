#!/usr/bin/env python
"""Look at linux-cve-announce for files touched by a given CVE."""

from argparse import ArgumentParser
from argparse import Namespace
import os
import re

from cki_lib import logger
from cki_lib import misc
from cki_lib.owners import Subsystem
# GitPython
import git
from jira.resources import Issue
import sentry_sdk

from webhook import kgit
from webhook.common import get_owners_parser
from webhook.libjira import JiraField
from webhook.libjira import fetch_issues

LOGGER = logger.get_logger('webhook.utils.cve_assigner')


def _get_parser_args():
    parser = ArgumentParser(description='Try to figure out default component for a given CVE')

    # Global options
    parser.add_argument('-c', '--cve', help='CVE to look up in linux-cve-announce.', required=True)
    parser.add_argument('-p', '--public-inbox', default=os.environ.get('PUBLIC_INBOX', ''),
                        help='Directory containing linux-cve-announce public inbox git')
    parser.add_argument('-o', '--owners-yaml', default=os.environ.get('OWNERS_YAML', ''),
                        help='Path to the owners.yaml file')
    parser.add_argument('--sentry-ca-certs', default=os.environ.get('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    return parser.parse_args()


def get_patch_data(args: 'Namespace', commit: 'git.Commit') -> (list[str], str):
    """Get the file list and upstream commit ID from the linux-cve-announce email."""
    mbox_path = os.path.realpath(f'{args.public_inbox}/m')

    kgit.branch_copy(args.public_inbox, args.cve)
    kgit.checkout(args.public_inbox, f'{args.cve}')
    kgit.hard_reset(args.public_inbox, commit.hexsha)

    with open(mbox_path, 'r', encoding='ascii') as mbox:
        mbox_data = mbox.read().rstrip()

    LOGGER.debug("Got mbox data of:\n%s", mbox_data)

    save_line = False
    file_paths = []
    ucid = None
    fixpat = (r"^\s*.*[Ff]ixed in (?P<kver2>\d{1,2}\.\d{1,2}(\.\d{1,3})?) "
              r"with commit (?P<fixsha>[a-f0-9]{12})$")
    fix_regex = re.compile(fixpat)

    for line in mbox_data.splitlines():
        fixref = fix_regex.match(line)
        if fixref:
            # Only capture the very last one, which will be the mainline upstream commit ID
            ucid = fixref.group('fixsha')
            continue
        if line.startswith("The file(s) affected by this issue are:"):
            save_line = True
            continue
        if line.startswith("Mitigation"):
            save_line = False
            break
        if save_line and line != '':
            file_paths.append(line.strip())

    return file_paths, ucid


def restore_git_state(args: 'Namespace') -> None:
    """Clean up the git repo state."""
    kgit.checkout(args.public_inbox, 'master')
    kgit.branch_delete(args.public_inbox, args.cve)


def search_for_cve_info(repo: 'git.Repo', cve: str) -> ('git.Commit', bool):
    """Search for the given CVE in the public inbox git."""
    log_output = repo.git.log('--oneline', f'--grep={cve}')
    rejected = False
    commit = None
    for line in log_output.splitlines():
        commit_sha = line.split()[0]
        commit_subj = ' '.join(line.split()[1:])
        if commit_subj.startswith(f'REJECTED: {cve}'):
            rejected = True
        elif commit_subj.startswith(f'{cve}'):
            commit = repo.commit(commit_sha)

    return commit, rejected


def parse_jira_data(args: 'Namespace', sst: 'Subsystem') -> list[Issue]:
    """Get jira data for the given CVE."""
    jira_data = fetch_issues([args.cve], filter_kwf=True)
    if not jira_data:
        LOGGER.info("No jira issues found for %s", args.cve)
        return []

    LOGGER.debug("Jira data: %s", jira_data)
    issues_to_update = []
    for issue in jira_data:
        if issue.fields.components[0].name == 'kernel-rt':
            LOGGER.debug("Ignoring %s kernel-rt clone %s", args.cve, issue.key)
        elif issue.fields.components[0].name != sst.jira_component:
            LOGGER.info("Issue %s component mismatch.\nJira: '%s'\nDocs: '%s'",
                        issue.key, issue.fields.components[0].name, sst.jira_component)
            issues_to_update.append(issue)
        else:
            LOGGER.debug("Issue %s components match: '%s'", issue.key, sst.jira_component)

    LOGGER.info("Issues to update: %s", issues_to_update)
    return issues_to_update


def update_jira_data(issues: list[Issue], sst: 'Subsystem', ucid: str) -> None:
    """Set the component to sst.jira_component for the given issues, add upstream commit ID."""
    # Checkbox IDs in the Reset_Contacts field for Assignee, QA Contact, Doc Contact,
    # Pool Team, Watchers and Developer. pme-bot will reset fields after component reassignment.
    contact_checkboxes = [{"id": "32051"}, {"id": "32052"}, {"id": "32053"},
                          {"id": "32054"}, {"id": "32055"}, {"id": "32850"}]
    for issue in issues:
        LOGGER.info("Setting issue %s component to %s, commit hashes to %s",
                    issue.key, sst.jira_component, ucid)
        if misc.is_production_or_staging():
            issue.update(fields={JiraField.components: [{"name": sst.jira_component}],
                                 JiraField.Commit_Hashes: ucid},
                         update={JiraField.Reset_Contacts: [{'set': contact_checkboxes}]})


def main(args: 'Namespace') -> None:
    """Find open MRs, check for relevant upstream Fixes."""
    if not args.public_inbox:
        LOGGER.warning("No valid public inbox git found, aborting!")
        return

    if not args.cve:
        LOGGER.warning("No valid CVE provided, aborting!")
        return

    owners = get_owners_parser(args.owners_yaml)

    LOGGER.debug("Fetching latest upstream mbox data...")
    kgit.checkout(args.public_inbox, 'master')
    kgit.fetch_all(args.public_inbox)
    kgit.hard_reset(args.public_inbox, 'origin/master')
    repo = git.Repo(args.public_inbox)

    commit, rejected = search_for_cve_info(repo, args.cve)

    if rejected:
        LOGGER.warning("%s was REJECTED upstream", args.cve)

    if commit is None:
        LOGGER.warning("No commit found, exiting.")
        return

    LOGGER.debug("Entry:\ncommit: %s\nsubject: %s", commit, commit.summary)

    paths, ucid = get_patch_data(args, commit)
    LOGGER.info("Got upstream commit of %s, path(s) of %s", ucid, paths)
    restore_git_state(args)
    subsystems = owners.get_matching_subsystems(paths)
    if not subsystems:
        LOGGER.warning("Unable to find a matching subsystem for paths %s in %s",
                       paths, args.owners_yaml)
        return

    sst = owners.get_matching_subsystems(paths)[0]
    LOGGER.debug("Got subsystem of %s for path(s) %s", sst, paths)
    LOGGER.debug("Got UCID of %s", ucid)
    LOGGER.debug("%s's SST: %s, Jira Component mapping: '%s'",
                 sst, sst.devel_sst, sst.jira_component)

    issues_to_update = parse_jira_data(args, sst)
    update_jira_data(issues_to_update, sst, ucid)


if __name__ == '__main__':
    pargs = _get_parser_args()
    misc.sentry_init(sentry_sdk, ca_certs=pargs.sentry_ca_certs)
    main(pargs)
