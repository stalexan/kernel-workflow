"""Git repo manipulation/check functions used by kernel webhooks."""
from datetime import datetime
from datetime import timedelta
import os
import subprocess

from cki_lib import logger
from cki_lib import messagequeue

LOGGER = logger.get_logger('cki.webhook.kgit')


def _git(src_dir, args, **kwargs):
    """Run a git via subprocess (for things GitPython doesn't support)."""
    kwargs.setdefault('check', True)
    # pylint: disable=subprocess-run-check
    LOGGER.debug("Running command: git %s", " ".join(args))
    return subprocess.run(['git'] + args, cwd=src_dir,
                          capture_output=True, text=True, **kwargs)


def branch_copy(location, branch):
    """Save a copy of the branch for later use w/git reset."""
    _git(location, ['branch', '--copy', branch])


def checkout(location, branch):
    """Reset the current checkout in location to branch."""
    _git(location, ['checkout', branch])


def branches_differ(location, branch_a, branch_b):
    """Compare two branches, return True if branches differ."""
    ret = _git(location, ['diff', '--stat', branch_a, branch_b])
    return bool(ret.stdout)


def hard_reset(location, branch):
    """Reset the current checkout in location to branch."""
    _git(location, ['reset', '--hard', branch])


def merge(location, reference):
    """Merge reference (quietly, with on commit message editing) to location."""
    _git(location, ['merge', '--quiet', '--no-edit', reference])


def cherry_pick(location, reference):
    """Cherry-pick the given commit to location, with cherry-picked ref and signoff."""
    _git(location, ['cherry-pick', '-x', '--signoff', reference])


def branch_delete(location, branch):
    """Delete git branch with extreme prejudice."""
    _git(location, ['branch', '-D', branch])


def remotes(location):
    """Fetch and return list of git remotes."""
    return _git(location, ['remote'], encoding='utf8').stdout.splitlines()


def pull_remote_branch(location, remote_name, branch):
    """Do a git pull on remote/branch."""
    _git(location, ['pull', remote_name, branch])


def fetch_remote(location, remote_name):
    """Fetch one git remote by name."""
    try:
        _git(location, ['fetch', remote_name])
    except subprocess.CalledProcessError as err:
        LOGGER.warning('Fetch of %s failed: %s', remote_name, err)


def fetch_all(location):
    """Do a fetch of all remotes."""
    git_remotes = remotes(location)
    for remote_name in git_remotes:
        fetch_remote(location, remote_name)


def prune_remote(location, remote):
    """Prune stale refs from given remote."""
    _git(location, ['remote', 'prune', remote])


def worktree_add(location, branch, worktree, target_branch):
    """Add worktree and branch."""
    _git(location, ['worktree', 'add', '-B', branch, worktree, target_branch])


def worktree_remove(location, worktree):
    """Delete worktree and branch."""
    _git(location, ['worktree', 'remove', '-f', worktree])


def setup_git_user(location, name, email):
    """Configure git user name and email address."""
    _git(location, ['config', 'user.name', name])
    _git(location, ['config', 'user.email', email])


def branch_mergeable(worktree_dir, target_branch, merge_branch):
    """Ensure MR branch can be merged to target branch."""
    hard_reset(worktree_dir, target_branch)
    try:
        merge(worktree_dir, merge_branch)
    except subprocess.CalledProcessError:
        return False

    return True


def create_worktree_timestamp(worktree_dir):
    """Create a timestamp file in the worktree."""
    now = datetime.now()
    ts_file = f"{worktree_dir}/timestamp"
    with open(ts_file, 'w', encoding='ascii') as ts_fh:
        ts_fh.write(now.strftime('%Y%m%d%H%M%S'))
        ts_fh.close()


def clean_up_temp_merge_branch(src_dir, merge_branch, worktree_dir):
    """Clean up the git worktree and branches from the test merges."""
    worktree_remove(src_dir, worktree_dir)
    branch_delete(src_dir, merge_branch)
    LOGGER.debug("Removed worktree %s and deleted branch %s", worktree_dir, merge_branch)


def handle_stale_worktree(src_dir, merge_branch, worktree_dir):
    """Ensure we're not stomping on another run, and clean it up if it's old."""
    LOGGER.warning("Worktree already exists at %s!", worktree_dir)
    ts_file = f"{worktree_dir}/timestamp"
    if not os.path.exists(ts_file):
        create_worktree_timestamp(worktree_dir)

    with open(ts_file, 'r', encoding='ascii') as ts_fh:
        old_ts = ts_fh.read()
        ts_fh.close()

    if old_ts < (datetime.now() - timedelta(hours=1)).strftime('%Y%m%d%H%M%S'):
        LOGGER.warning("Stale worktree at %s is over 1h old, purging it.", worktree_dir)
        clean_up_temp_merge_branch(src_dir, merge_branch, worktree_dir)
        try:
            branch_delete(worktree_dir, f"{merge_branch}-save")
        except subprocess.CalledProcessError:
            return
    else:
        raise messagequeue.QuietNackException("Worktree less than 1h old, trying back later.")


def prep_temp_merge_branch(remote_name, gl_mergerequest, src_dir):
    """Set up the temporary branch/worktree to test mergeability in."""
    # This target_branch is dependent on the repo layout in utils/rh_kernel_git_repos.yml,
    # where everything is an additional remote on top of kernel-ark, w/remote name == project name
    target_branch = f"{remote_name}/{gl_mergerequest.target_branch}"
    merge_branch = f"{remote_name}-{gl_mergerequest.target_branch}-{gl_mergerequest.iid}"
    worktree_dir = f"/{'/'.join(src_dir.strip('/').split('/')[:-1])}/{merge_branch}-merge/"

    # Ensure we don't have a stale/failed checkout lingering
    if os.path.isdir(worktree_dir):
        handle_stale_worktree(src_dir, merge_branch, worktree_dir)

    # This is the lookaside cache we maintain for examining diffs between revisions of a
    # merge request, which we're going to create temporary worktrees off of
    LOGGER.info("Creating git worktree at %s with branch %s for mergeability testing, please hold",
                worktree_dir, merge_branch)
    worktree_add(src_dir, merge_branch, worktree_dir, target_branch)
    return merge_branch, worktree_dir
