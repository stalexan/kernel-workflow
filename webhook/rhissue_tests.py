"""Readiness tests for RHIssue objects."""
# pylint: disable=invalid-name
from functools import wraps
import typing

from cki_lib.logger import get_logger

from webhook.defs import JIPTStatus
from webhook.defs import JIPriority
from webhook.defs import JIResolution
from webhook.defs import JIStatus
from webhook.defs import KWF_SUPPORTED_ISSUE_TYPES
from webhook.defs import MrScope
from webhook.defs import MrState
from webhook.libjira import JiraField
from webhook.libjira import fetch_issues

if typing.TYPE_CHECKING:
    from webhook.rhissue import RHIssue

LOGGER = get_logger('cki.webhook.rhissue_tests')


# Decorator function that runs each test.
def test_runner(passed=MrScope.READY_FOR_MERGE, failed=MrScope.IN_PROGRESS, keep_going=True,
                skip_if_failed=None):
    """Decorator to run one RHIssue Test."""
    def decorate_test(func):
        @wraps(func)
        def run_test(*args, **kwargs):
            rhissue = kwargs['rhissue'] if 'rhissue' in kwargs else args[0]
            f_name = func.__name__

            if skip_if_failed and any(rhissue.test_failed(test) for test in skip_if_failed):
                LOGGER.debug('Passing %s test as it has failed one of these tests: %s',
                             f_name, skip_if_failed)
                result = True
                result_str = 'skipped'
            else:
                result = func(*args, **kwargs)
                result_str = 'passed' if result else 'failed'
            if not result:
                rhissue.failed_tests.append(f_name)

            scope = passed if result else failed
            LOGGER.debug('[%s] %s %s, scope is: %s', rhissue.alias, f_name, result_str, scope.name)
            return result, scope, result if not keep_going else True
        return run_test
    return decorate_test


def has_rt_variant(rhissue: "RHIssue") -> bool:
    """Check if there is a kernel-rt variant of an issue.

    Given a CVE issue for stock kernel, search for the kernel-rt
    equivalent of it.
    """
    assert rhissue.ji_branch
    # consult rh_metadata to check if kernel-rt is a component of this branch
    if "kernel-rt" not in rhissue.ji_branch.components:
        return True

    issues = [
        ji
        for ji in fetch_issues(rhissue.ji_cves)
        if ji.ji_fix_version == rhissue.ji_fix_version
        and "kernel-rt" in ji.ji_components
    ]
    return bool(issues)


@test_runner()
def ParentCommitsMatch(rhissue):
    """Pass if the commits referencing the RHIssue in the parent MR and Dep MR match."""
    # If the dependency MR is merged then the dependant (parent) MR shouldn't have any commits which
    # reference this RHIssue... unless it needs to be rebased >:( ...
    if rhissue.mr.state is MrState.MERGED:
        return not rhissue.parent_mr_commits
    # ... otherwise the commits which reference the RHIssue should be the same!
    return rhissue.commits == rhissue.parent_mr_commits


ParentCommitsMatch.note = ("The commit SHAs referencing this JIRA Issue in this MR do not match "
                           "the commit SHAs referencing this JIRA Issue in the Dependency MR.  "
                           "This indicates this MR is based upon an older version of the "
                           "Dependency MR.")


@test_runner(failed=MrScope.READY_FOR_MERGE, keep_going=False,
             skip_if_failed=['ParentCommitsMatch'])
def MRIsNotMerged(rhissue):
    """Pass if the Dependency RHIssue's MR is not merged, otherwise "Fail"."""
    # Failing this isn't bad, it just gets us a note.
    return rhissue.mr.state is not MrState.MERGED


MRIsNotMerged.note = "The MR associated with this Dependency JIRA Issue is already merged. Great."


@test_runner(failed=MrScope.MISSING)
def InMrDescription(rhissue):
    """Pass if the RHIssue appears in the MR Description, otherwise Fail."""
    # Skip this test for any RHIssue that doesn't have commits such as the dummy UNTAGGED RHIssue.
    return rhissue.in_mr_description if rhissue.commits else True


InMrDescription.note = ("These commits have a `JIRA:` or `CVE:` tag which references "
                        "a JIRA Issue or CVE ID that was not listed in the merge request "
                        "description.  Please verify the tag is correct, or, add them to your MR "
                        "description as either a `JIRA`, `CVE`, or `Depends` tag.")


@test_runner(failed=MrScope.MISSING)
def HasCommits(rhissue):
    """Pass if the RHIssue is tagged in some commits, otherwise Fail."""
    return bool(rhissue.commits)


HasCommits.note = ("This tag is referenced in the MR description but is not referenced in "
                   "any of the MR's commits.  Please ensure the tag is correct and update "
                   "commit descriptions with `JIRA`, `Depends`, or `CVE` tags as needed.")


@test_runner(failed=MrScope.INVALID, keep_going=False)
def JIisNotUnknown(rhissue):
    """Pass if the RHIssue status is not UNKNOWN, otherwise Fail."""
    # On failure sets MR scope to INVALID and stops testing.
    return rhissue.ji_status is not JIStatus.UNKNOWN


JIisNotUnknown.note = ("There was a problem retrieving data from JIRA. Please check "
                       "that the JIRA information is correct and contact a maintainer "
                       "if the problem persists.")


@test_runner(failed=MrScope.INVALID, keep_going=False)
def JIisNotWrongType(rhissue):
    """Pass if the RHIssue is a support issue type, otherwise Fail."""
    # On failure sets MR scope to INVALID and stops testing.
    return rhissue.ji_type in KWF_SUPPORTED_ISSUE_TYPES


JIisNotWrongType.note = ("This JIRA Issue is some type other than a 'Bug' or 'Story'. The kernel "
                         "workflow only supports 'Bug' & 'Story' issues. Please reference a "
                         "different issue.")


@test_runner(failed=MrScope.CLOSED, keep_going=False)
def JIisNotClosed(rhissue):
    """Pass if the RHIssue status is not CLOSED, otherwise Fail."""
    # On failure sets MR scope to INVALID and stops testing.
    return rhissue.ji_status is not JIStatus.CLOSED


JIisNotClosed.note = ("This JIRA Issue's status is CLOSED.  Please check that the JIRA "
                      "information is correct.")


@test_runner(failed=MrScope.NEW)
def JIisNotNewMRisDraft(rhissue):
    """Pass if RHIssue status is not new or MR is not in Draft."""
    return not (rhissue.ji_status is JIStatus.NEW and rhissue.mr.is_draft)


JIisNotNewMRisDraft.note = "This JIRA Issue's status is New and the MR is in draft."


@test_runner(failed=MrScope.MISSING, keep_going=False)
def NotUntagged(rhissue):
    """Pass if the RHIssue is not the faux 'UNTAGGED' rhissue."""
    # On failure stops testing
    return not rhissue.untagged


NotUntagged.note = ("No `JIRA` tag was found in these commits.  This project requires that "
                    "each commit have at least one `JIRA` tag.  Please double-check the tag "
                    "formatting and/or add a `JIRA: <issue_URL>` tag for each JIRA Issue.")


@test_runner(failed=MrScope.NEEDS_REVIEW, keep_going=True)
def JIhasPriority(rhissue):
    """
    If RHIssue is assigned to a CVE pass only if it has a Priority set.

    If RHIssue is not assigned to a CVE, pass always
    """
    return True if len(rhissue.ji_cves) == 0 else (not rhissue.ji_priority == JIPriority.UNKNOWN)


JIhasPriority.note = ("This JIRA issue has no priority set. If a Jira related to a CVE has "
                      "no priority set, then it is not possible to attach it to an errata. "
                      "Please, make sure this JIRA issue has a Priority set.")


@test_runner(failed=MrScope.NEEDS_REVIEW)
def CveInMrDescription(rhissue):
    """Pass if all CVEs associated with the RHIssue are tagged in the MR Description."""
    result = True
    for cve_id in rhissue.ji_cves:
        if cve := next((cve for cve in rhissue.mr.cves if cve_id in cve.cve_ids), None):
            if cve.in_mr_description:
                continue
        result = False
        break
    return result


CveInMrDescription.note = ("This JIRA Issue is for a CVE that was not listed in the merge request "
                           "description.  Please verify the Issue's URL is correct or add the "
                           "CVE(s) to the MR description as a `CVE: CVE-YYYY-XXXXX` tag.")


# This test is Not Good because the graphql API does not expose per-commit file lists. So if an
# MR has a JIRA: INTERNAL tag anywhere then we can only validate whether the entire MR
# touches only internal files or not.
@test_runner(failed=MrScope.INVALID)
def IsValidInternal(rhissue):
    """Pass if the RHIssue is a valid INTERNAL_JISSUE or not internal, otherwise Fail."""
    return rhissue.mr.only_internal_files if rhissue.internal else True


IsValidInternal.note = ("These commits are tagged as `INTERNAL` but were found to touch "
                        "source files outside of the redhat/ directory.  `INTERNAL` rhissues "
                        "are only to be used for changes to files in the redhat/ directory.")


@test_runner(failed=MrScope.PLANNING)
def TargetReleaseSet(rhissue):
    """Pass if Fix Version is set, otherwise fail."""
    if not rhissue.ji:
        return True
    return bool(rhissue.ji_fix_version)


TargetReleaseSet.note = "This JIRA Issue does not have a valid Fix Version set."


@test_runner(failed=MrScope.NEEDS_REVIEW)
def IsAssigned(rhissue: 'RHIssue') -> bool:
    """Pass if the jira issue is assigned to a user, otherwise Fail."""
    return rhissue.assignee is not None


IsAssigned.note = "This JIRA issue is not assigned to anyone. Please make sure it is assigned."


@test_runner(failed=MrScope.NEEDS_REVIEW)
def HasQAContact(rhissue: 'RHIssue') -> bool:
    """Pass if the jira issue has a QA Contact, otherwise Fail."""
    return rhissue.qa_contact is not None


HasQAContact.note = ("This JIRA issue does not have a QA Contact assigned. Please work with "
                     "the relevant SST and Kernel QE to have a QA Contact assigned.")


@test_runner(failed=MrScope.NEEDS_REVIEW)
def IsNotTestOnly(rhissue: 'RHIssue') -> bool:
    """Pass if the jira issue does *not* have the 'TestOnly' keyword, otherwise Fail."""
    return 'TestOnly' not in rhissue.keywords


IsNotTestOnly.note = ("This JIRA issue has the 'TestOnly' keyword set.  'TestOnly' issues should"
                      " not be used to track code changes and cannot be attached to errata."
                      "  Either remove the 'TestOnly' keyword from the issue or do not reference"
                      " this issue in any MR or commits.")


@test_runner(failed=MrScope.PLANNING)
def DevApproved(rhissue):
    """Pass if the jira issue has a DTM set, but only for y-stream."""
    if not rhissue.ji_fix_version or rhissue.ji_fix_version.zstream:
        return True
    dev_dtm = getattr(rhissue.ji.fields, JiraField.Dev_Target_Milestone)
    return dev_dtm is not None


DevApproved.note = ("The JIRA Issue associated with these commits does not have a Dev Target "
                    "Milestone set. Set it yourself, or work with your PO/SST to get it set.")


@test_runner(failed=MrScope.PLANNING)
def QEApproved(rhissue):
    """Pass if the jira issue has an ITM set, but only for y-stream."""
    if not rhissue.ji_fix_version or rhissue.ji_fix_version.zstream:
        return True
    qe_itm = getattr(rhissue.ji.fields, JiraField.Internal_Target_Milestone)
    return qe_itm is not None


QEApproved.note = ("The JIRA Issue associated with these commits does not have an Internal Target "
                   "Milestone set. Talk to your QE counterpart to get it set.")


@test_runner()
def CommitPolicyApproved(rhissue):
    """Pass if the rhissue commit policy check passed, otherwise Fail."""
    return bool(rhissue.policy_check_ok[0])


CommitPolicyApproved.note = ("The JIRA Issue associated with these commits is not approved at "
                             "this time.")


@test_runner(failed=MrScope.PLANNING, skip_if_failed=['CommitPolicyApproved'])
def NotPrelimTestingFail(rhissue):
    """Return True if the JIRA Issue has Preliminary Testing: Fail, otherwise False."""
    # Skip this test if CommitPolicyApproved failed.
    return bool(rhissue.ji_pt_status != JIPTStatus.FAIL)


NotPrelimTestingFail.note = ("The JIRA Issue associated with these commits has failed "
                             "preverification testing. Please re-plan this Issue.")


@test_runner(failed=MrScope.READY_FOR_QA,
             skip_if_failed=['CommitPolicyApproved', 'NotPrelimTestingFail'])
def PrelimTestingPass(rhissue):
    """Return True if the JIRA Issue has Preliminary Testing: Pass, otherwise False."""
    # Skip this test if CommitPolicyApproved failed.
    return bool(rhissue.ji_pt_status == JIPTStatus.PASS)


PrelimTestingPass.note = ("The JIRA Issue associated with these commits has not passed "
                          "preverification at this time.")


@test_runner(skip_if_failed=['TargetReleaseSet'], failed=MrScope.PLANNING)
def CentOSZStream(rhissue):
    """
    Pass if this is not the c9s project or the MR branch and JIRA Issue branch match.

    Fail if this is c9s and the ji_branch cannot be found or it has ZTR set.
    """
    result = True
    if rhissue.is_dependency or rhissue.mr.project.name != 'centos-stream-9' or not rhissue.ji:
        pass
    elif not rhissue.ji_branch or rhissue.ji_branch.zstream_target_release:
        result = False
    return result


CentOSZStream.note = ("This JIRA Issue targets a zstream release but this MR exists in the Centos "
                      "Stream project.  Centos Stream Issues are expected to target ystream.  "
                      "This MR may need to be recreated in the RHEL9 project; please "
                      "contact a maintainer for further assistance.")


@test_runner(skip_if_failed=['CentOSZStream'])
def ComponentMatches(rhissue):
    """Pass if any of the RHIssue components matches the MR target branch, otherwise Fail."""
    if not rhissue.ji:
        return True
    allowed_components = rhissue.mr.branch.components
    # Prior to 9.3 each Branch has a single component, but for CVEs we want to allow tagging of
    # variant issues in the MR so accept any of the standard kernel components.
    if rhissue.ji_cves and float(rhissue.mr.branch.version) < 9.3:
        allowed_components = {'kernel', 'kernel-automotive', 'kernel-rt'}
    return bool(rhissue.ji_components & allowed_components)


ComponentMatches.note = ("This JIRA Issue does not have any 'components' which match the MR's "
                         "target branch.  'kernel' Issues must have an MR which targets a kernel "
                         "branch and 'kernel-rt' Issue MRs must target a kernel-rt branch.")


@test_runner(skip_if_failed=['TargetReleaseSet', 'CentOSZStream', 'ComponentMatches'],
             failed=MrScope.PLANNING)
def BranchMatches(rhissue):
    """Pass if the RHIssue's MR Branch version matches the RHIssue Branch version."""
    if not rhissue.ji_branch or not rhissue.mr.branch:
        return False
    return rhissue.ji_branch.version == rhissue.mr.branch.version


BranchMatches.note = ("This JIRA Issue has a Fix Version/s value that does not correspond to "
                      "the target branch of this MR.  Please review the Issue and the MR target "
                      "branch to ensure they are correct.")


@test_runner(failed=MrScope.INVALID)
def ComponentIsKernel(rhissue) -> bool:
    """Check if the jira issue component is kernel/*."""
    return any(c.startswith('kernel') for c in rhissue.ji_components)


ComponentIsKernel.note = "This JIRA issue doesn't have a 'kernel/' component."


@test_runner()
def CvePriority(cve):
    """Pass if the CVE priority is >= Major and lead stream RHIssue is on errata, otherwise Fail."""
    if cve.ji_priority < JIPriority.MAJOR:
        return True
    parent_branch = cve.parent_mr.branch
    # Make a reversed copy of the CVE's ji_depends_on list since we want the 'highest' branch first.
    cve_clones = list(reversed(cve.ji_depends_on))
    # The lead stream clone for rhel-6 & rhel-7 is the one associated with the 'main' branch,
    # otherwise it is the first one not associated with the 'main' branch.
    LOGGER.warning("Parent branch project: %s", parent_branch.project.name)
    if parent_branch.project.name in ('rhel-6', 'rhel-7'):
        lead_clone = next((rhissue for rhissue in cve_clones if rhissue.ji_branch.name == 'main'))
    else:
        lead_clone = next((rhissue for rhissue in cve_clones if rhissue.ji_branch.name != 'main'))
    # The clone associated with the MR.
    parent_clone = next((rhissue for rhissue in cve_clones if rhissue.ji_branch == parent_branch))
    if lead_clone.ji_branch > parent_clone.ji_branch and \
       lead_clone.ji_resolution != JIResolution.ERRATA:
        LOGGER.warning("lcb: %s, pcb: %s, lcr: %s", lead_clone.ji_branch, parent_clone.ji_branch,
                       lead_clone.ji_resolution)
        return False
    return True


CvePriority.note = "This CVE is not yet in errata for the lead stream."


@test_runner()
def CveKernelRTVariant(rhissue: 'RHIssue') -> bool:
    """Check if there is a kernel-rt variant of the issue."""
    return has_rt_variant(rhissue)


CveKernelRTVariant.note = "A kernel-rt variant of this CVE for this release was not found."


# Tests to run on JIRA: Issues
ISSUE_TESTS = [InMrDescription,
               HasCommits,
               JIisNotUnknown,
               JIisNotWrongType,
               JIisNotClosed,
               ComponentIsKernel,
               JIhasPriority,
               TargetReleaseSet,
               CveInMrDescription,
               IsValidInternal,
               IsAssigned,
               HasQAContact,
               IsNotTestOnly,
               DevApproved,
               QEApproved,
               JIisNotNewMRisDraft,
               CommitPolicyApproved,
               NotPrelimTestingFail,
               PrelimTestingPass,
               CentOSZStream,
               ComponentMatches,
               BranchMatches
               ]

# Tests to run on linked Issues
LISSUE_TESTS = [JIisNotUnknown,
                JIisNotWrongType,
                JIisNotClosed,
                JIhasPriority,
                TargetReleaseSet,
                IsAssigned,
                HasQAContact,
                IsNotTestOnly,
                DevApproved,
                QEApproved,
                CommitPolicyApproved,
                NotPrelimTestingFail,
                PrelimTestingPass,
                CentOSZStream,
                ComponentMatches,
                BranchMatches
                ]

# Tests to run on Depends: Issues
DEP_TESTS = [ParentCommitsMatch,
             MRIsNotMerged,
             InMrDescription,
             HasCommits,
             JIisNotUnknown,
             JIisNotWrongType,
             JIisNotClosed,
             TargetReleaseSet,
             IsValidInternal,
             IsAssigned,
             HasQAContact,
             IsNotTestOnly,
             DevApproved,
             QEApproved,
             JIisNotNewMRisDraft,
             CommitPolicyApproved,
             NotPrelimTestingFail,
             PrelimTestingPass,
             CentOSZStream,
             ComponentMatches,
             ]

# Tests to run on CVE: Issues
CVE_TESTS = [InMrDescription,
             HasCommits,
             JIisNotUnknown,
             CveKernelRTVariant,
             # JIisNotClosed,
             # CvePriority
             ]

INTERNAL_TESTS = [InMrDescription,
                  HasCommits,
                  IsValidInternal]

UNTAGGED_TESTS = [NotUntagged]
