"""Webhook interaction tests."""
from subprocess import CalledProcessError
from subprocess import CompletedProcess
import typing
from unittest import mock

from tests.helpers import KwfTestCase
from webhook import defs
from webhook import mergehook
from webhook.session import SessionRunner
from webhook.session_events import create_event


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestMergehook(KwfTestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._mocked_runs = []
        self._mocked_calls = []

    def _mock_run(self, args, *, check=False, **_):
        for run in self._mocked_runs:
            if run[0] == args:
                (returncode, stdout) = run[1:]
                break
        else:
            self.fail(f'Command {args} not found in mocked subprocess.run')
        self._mocked_calls.append(args)
        if returncode:
            raise CalledProcessError(returncode, args, output=stdout)
        return CompletedProcess(args, returncode, stdout=stdout)

    def _add_run_result(self, args, returncode, stdout=None):
        self._mocked_runs.append((args, returncode, stdout))

    def test_find_orig_mr_fileset(self):
        orig_mr_id = '777'
        mr_list = [{'iid': '777',
                    'author': {'username': 'shadowman'},
                    'title': 'This is the base MR',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'files': [{'path': 'some/file.c'}]
                    },
                   {'iid': '876',
                    'author': {'username': 'shadowman'},
                    'title': 'This is a test of the emergency mergecast system',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'files': [{'path': 'some/file.c'}]
                    }]
        result = mergehook.find_orig_mr_fileset(mr_list, orig_mr_id)
        self.assertEqual(result, {'some/file.c'})

    @mock.patch('webhook.mergehook.find_orig_mr_fileset')
    def test_fetch_mr_list(self, mock_orig_files):
        mock_orig_files.return_value = {'some/file.c'}
        mock_proj = mock.Mock()
        mock_proj.path_with_namespace = 'cki-project/kernel-ark'
        mock_mr = mock.Mock()
        mock_mr.iid = '777'
        mock_mr.target_branch = 'os-build'
        mock_graphql = mock.Mock()
        mr_list = [{'iid': '876',
                    'author': {'username': 'shadowman'},
                    'title': 'This is a test of the emergency mergecast system',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'files': [{'path': 'some/file.c'}]
                    }]
        qres = {'project':
                {'id': 'gid://gitlab/Project/13604247',
                 'mergeRequests':
                 {'pageInfo': {'hasNextPage': False, 'endCursor': 'xyz'},
                  'nodes': mr_list
                  }
                 }
                }
        mock_graphql.check_query_results.return_value = qres
        result = mergehook.fetch_mr_list(mock_proj, mock_mr, mock_graphql)
        self.assertEqual(result, mr_list)
        mock_orig_files.return_value = {}
        with self.assertLogs('cki.webhook.mergehook', level='WARNING') as logs:
            result = mergehook.fetch_mr_list(mock_proj, mock_mr, mock_graphql)
            self.assertIn("Originating MR 777 appears to have no files", logs.output[-1])
            self.assertEqual(result, [])

    @mock.patch('webhook.mergehook.MRDescription')
    @mock.patch('webhook.mergehook.fetch_mr_list')
    @mock.patch('webhook.kgit.branch_copy', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.branch_mergeable', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.hard_reset', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.branch_delete', mock.Mock(return_value=True))
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_check_for_other_merge_conflicts_ok(self, mock_fmrl, mock_mrdesc):
        mock_proj = mock.Mock()
        mock_proj.name = 'kernel-ark'
        mock_mr = mock.Mock()
        mock_mr.iid = 66
        mock_gql = mock.Mock()
        mock_mrdesc.return_value = mock.Mock(depends_mrs={})
        merge_branch = 'temp-merge-branch'
        worktree_dir = '/src/kernel-ark-temp-merge-branch/'
        mr_list = [{'iid': '44',
                    'author': {'username': 'shadowman'},
                    'title': 'Update the kernel release name',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'labels': {'nodes': [{'title': 'blah'}]}
                    },
                   {'iid': '55',
                    'author': {'username': 'joedev'},
                    'title': 'A terrible misguided commit without Signof-by',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'labels': {'nodes': [{'title': 'blah'}]}
                    },
                   {'iid': '66',
                    'author': {'username': 'rhnewbie'},
                    'title': 'My first MR!',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'labels': {'nodes': [{'title': 'blah'}]}
                    }
                   ]
        mock_fmrl.return_value = mr_list
        m44_args = ['git', 'merge', '--quiet', '--no-edit',
                    f"{mock_proj.name}/merge-requests/{mr_list[0]['iid']}"]
        m55_args = ['git', 'merge', '--quiet', '--no-edit',
                    f"{mock_proj.name}/merge-requests/{mr_list[1]['iid']}"]
        m66_args = ['git', 'merge', '--quiet', '--no-edit',
                    f"{mock_proj.name}/merge-requests/{mr_list[1]['iid']}"]
        self._add_run_result(m44_args, 0)
        self._add_run_result(m55_args, 0)
        self._add_run_result(m66_args, 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.check_for_other_merge_conflicts(mock_proj, mock_mr, mock_gql,
                                                            merge_branch, worktree_dir,
                                                            mock_proj.name)
            self.assertEqual(ret, [])

    @mock.patch('webhook.mergehook.MRDescription')
    @mock.patch('webhook.kgit.branch_mergeable')
    @mock.patch('webhook.mergehook.fetch_mr_list')
    @mock.patch('webhook.kgit.branch_copy', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.hard_reset', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.branch_delete', mock.Mock(return_value=True))
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_check_for_other_merge_conflicts_bad(self, mock_fmrl, mock_mergeable, mock_mrdesc):
        mock_proj = mock.Mock()
        mock_proj.name = 'kernel-ark'
        mock_mr = mock.Mock()
        mock_mr.iid = 66
        mock_gql = mock.Mock()
        mock_mrdesc.return_value = mock.Mock(depends_mrs={})
        merge_branch = 'temp-merge-branch'
        worktree_dir = '/src/kernel-ark-temp-merge-branch/'

        mr_list = [{'iid': '44',
                    'author': {'username': 'shadowman'},
                    'title': 'Update the kernel release name',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'labels': {'nodes': [{'title': defs.MERGE_CONFLICT_LABEL}]}
                    }]
        mock_fmrl.return_value = mr_list
        m_args = ['git', 'merge', '--quiet', '--no-edit',
                  f"{mock_proj.name}/merge-requests/{mr_list[0]['iid']}"]

        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.check_for_other_merge_conflicts(mock_proj, mock_mr, mock_gql,
                                                            merge_branch, worktree_dir,
                                                            mock_proj.name)
            self.assertNotIn(m_args, self._mocked_calls)
            self.assertEqual(ret, [])

        mr_list = [{'iid': '44',
                    'author': {'username': 'shadowman'},
                    'title': 'Update the kernel release name',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'labels': {'nodes': [{'title': 'blah'}]}
                    }]
        error_text = (f"MR !{mr_list[0]['iid']} from @{mr_list[0]['author']['username']} "
                      f"(`{mr_list[0]['title']}`) conflicts with this MR.  \n")
        mock_fmrl.return_value = mr_list
        m_args = ['git', 'merge', '--quiet', '--no-edit',
                  f"{mock_proj.name}/merge-requests/{mr_list[0]['iid']}"]

        mock_mergeable.return_value = False
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.check_for_other_merge_conflicts(mock_proj, mock_mr, mock_gql,
                                                            merge_branch, worktree_dir,
                                                            mock_proj.name)
            self.assertNotIn(m_args, self._mocked_calls)
            self.assertEqual(ret, [])

        conflict_note = ("CONFLICT: your dependency MR has different hashes from the ones "
                         "included in your MR. Please rebase this MR on top of the "
                         "current version of !44.")
        mock_mergeable.return_value = True
        mock_mrdesc.return_value = mock.Mock(depends_mrs={44})
        dep_link = 'https://gitlab.com/cki-project/kernel-ark/-/merge_requests/44'
        mock_mr.description = f'Depends: {dep_link}'
        self._add_run_result(m_args, 5, 'Catastrophic error!')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.check_for_other_merge_conflicts(mock_proj, mock_mr, mock_gql,
                                                            merge_branch, worktree_dir,
                                                            mock_proj.name)
            self.assertIn(m_args, self._mocked_calls)
            self.assertEqual(ret, [conflict_note, error_text, 'Catastrophic error!'])

    @mock.patch('webhook.kgit.create_worktree_timestamp', mock.Mock(return_value=True))
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_check_for_merge_conflicts_ok(self):
        remote_name = 'kernel-ark'
        gl_mergerequest = mock.Mock()
        gl_mergerequest.iid = 2
        gl_mergerequest.target_branch = 'os-build'
        mock_src = '/src/kernel-ark/'

        # Clean merges
        m_args = ['git', 'merge', '--quiet', '--no-edit',
                  f"{remote_name}/merge-requests/{gl_mergerequest.iid}"]
        self._add_run_result(m_args, 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.check_for_merge_conflicts(remote_name, gl_mergerequest, mock_src)
            self.assertIn(m_args, self._mocked_calls)
            self.assertEqual(ret, [])

    @mock.patch('webhook.kgit.create_worktree_timestamp', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.hard_reset', mock.Mock(return_value=True))
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_check_for_merge_conflicts_bad(self):
        remote_name = 'kernel-ark'
        gl_mergerequest = mock.Mock()
        gl_mergerequest.iid = 2
        gl_mergerequest.target_branch = 'os-build'
        mock_src = '/src/kernel-ark/'
        error_text = (f"MR !{gl_mergerequest.iid} cannot be merged to "
                      f"{remote_name}/{gl_mergerequest.target_branch}  \n")

        # Failed merge
        self._mocked_calls = []
        m_args = ['git', 'merge', '--quiet', '--no-edit',
                  f"{remote_name}/merge-requests/{gl_mergerequest.iid}"]
        self._add_run_result(m_args, 5, 'Catastrophic error!')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.check_for_merge_conflicts(remote_name, gl_mergerequest, mock_src)
            self.assertIn(m_args, self._mocked_calls)
            self.assertEqual(ret, [error_text, 'Catastrophic error!'])

    @mock.patch('webhook.kgit._git')
    @mock.patch('webhook.kgit.handle_stale_worktree', mock.Mock(return_value=True))
    @mock.patch('webhook.kgit.clean_up_temp_merge_branch', mock.Mock(return_value=True))
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment',
                mock.Mock(return_value=True))
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.mergehook.check_for_other_merge_conflicts')
    @mock.patch('webhook.mergehook.check_for_merge_conflicts')
    def test_process_merge_request(self, mock_conflicts, mock_other_conflicts,
                                   mock_add_label, mock_git):
        mock_inst = mock.Mock()
        mock_proj = mock.Mock()
        mock_proj.name = 'kernel-ark'
        mock_projects = mock.Mock()
        mock_projects.return_value.get_project_by_id.return_value = mock.Mock(confidential=False)
        mock_mr = mock.Mock(iid=66)
        mock_mr.target_branch = 'os-build'
        mock_gql = mock.Mock()
        mock_session = SessionRunner.new('mergehook', [], mergehook.HANDLERS)
        mock_session.get_gl_instance = mock.Mock(return_value=mock_inst)
        mock_session.get_graphql = mock.Mock(return_value=mock_gql)
        mock_session.get_gl_project = mock.Mock(return_value=mock_proj)
        mock_session.args = mock.Mock(rhkernel_src='/src/kernel-ark')
        mock_conflicts.return_value = ['MR !66 cannot be merged to os-build', 'CONFLICTS']
        mock_other_conflicts.return_value = ['CONFLICTS']
        mock_event = mock.Mock(
            mr_url=defs.GitlabURL('https://gitlab.com/cki-project/kernel-ark/-/merge_requests/66'),
            kind=defs.GitlabObjectKind.MERGE_REQUEST,
            namespace='cki-project/kernel-ark',
            gl_mr=mock_mr,
        )
        with mock.patch('webhook.mergehook.event_days_old', mock.Mock()) as patched_event_days_old:
            patched_event_days_old.return_value = 0
            mergehook.process_gl_event({}, mock_session, mock_event)
            mock_add_label.assert_called_with(mock_proj, 66, [defs.MERGE_CONFLICT_LABEL])

            mock_conflicts.return_value = []
            mergehook.process_gl_event({}, mock_session, mock_event)
            mock_add_label.assert_called_with(mock_proj, 66, [defs.MERGE_WARNING_LABEL])

            mock_other_conflicts.return_value = []
            mergehook.process_gl_event({}, mock_session, mock_event)
            mock_add_label.assert_called_with(mock_proj, 66, [f'Merge::{defs.READY_SUFFIX}'])


class TestEventDaysOld(KwfTestCase):
    """Test the event_days_old function."""

    class EventAgeTest(typing.NamedTuple):

        expected_days_old: int
        test_run_timestamp: str
        event_headers_timestamp: str
        event_body: dict
        event_body_mr_updated_at: str

    def run_test(self, test: 'TestEventDaysOld.EventAgeTest') -> None:
        """Run an EventAgeTest."""
        headers = {'message-type': 'gitlab'}
        if test.event_headers_timestamp:
            headers['message-date'] = test.event_headers_timestamp

        body = test.event_body or \
            self.load_yaml_asset('gl_merge_request_1.json', sub_module='webhook_events')
        if test.event_body_mr_updated_at:
            key = None
            if 'merge_request' in body:
                key = 'merge_request'
            elif 'object_attributes' in body:
                key = 'object_attributes'
            if key:
                body[key]['updated_at'] = test.event_body_mr_updated_at

        test_session = SessionRunner.new('mergehook', '', mergehook.HANDLERS)
        test_event = create_event(test_session, headers, body)

        self.freeze_time(test.test_run_timestamp)
        self.assertEqual(mergehook.event_days_old(test_event), test.expected_days_old)

    def test_event_header_ts_0_days_old(self) -> None:
        """Uses BaseEvent.timestamp to find event is 0 days old."""
        self.run_test(self.EventAgeTest(
            expected_days_old=0,
            test_run_timestamp='Fri 01 May 2024 12:00:00 UTC',
            event_headers_timestamp='2024-05-01T11:59:00+00:00',
            event_body={},
            event_body_mr_updated_at=''
        ))

    def test_event_header_ts_5_days_old(self) -> None:
        """Uses BaseEvent.timestamp to find event is 5 days old."""
        self.run_test(self.EventAgeTest(
            expected_days_old=5,
            test_run_timestamp='Fri 06 May 2024 12:00:00 UTC',
            event_headers_timestamp='2024-05-01T11:59:00+00:00',
            event_body={},
            event_body_mr_updated_at=''
        ))

    def test_event_body_updated_at_0_days_old(self) -> None:
        """Uses Event body to find event is 0 days old."""
        self.run_test(self.EventAgeTest(
            expected_days_old=0,
            test_run_timestamp='Fri 01 May 2024 12:00:00 UTC',
            event_headers_timestamp='',
            event_body={},
            event_body_mr_updated_at='2024-05-01 05:00:00 UTC'
        ))

    def test_event_body_updated_at_3_days_old(self) -> None:
        """Uses Event body to find event is 3 days old."""
        self.run_test(self.EventAgeTest(
            expected_days_old=3,
            test_run_timestamp='Fri 04 May 2024 12:00:00 UTC',
            event_headers_timestamp='',
            event_body={},
            event_body_mr_updated_at='2024-05-01 05:00:00 UTC'
        ))

    def test_no_timestamps(self) -> None:
        """Returns 0 when there are no working available timestamps."""
        self.run_test(self.EventAgeTest(
            expected_days_old=0,
            test_run_timestamp='Fri 04 May 2024 12:00:00 UTC',
            event_headers_timestamp='',
            event_body={},
            event_body_mr_updated_at='a very bad timestamp'
        ))
