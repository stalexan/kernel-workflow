"""Tests for the session_events module."""
from copy import deepcopy
from datetime import datetime
import typing
from unittest import mock

from tests import fake_payloads
from tests import fakes
from tests.helpers import KwfTestCase
from webhook import defs
from webhook import session_events
from webhook.common import get_arg_parser
from webhook.graphql import StateTuple
from webhook.session import SessionRunner

if typing.TYPE_CHECKING:
    from webhook.session_events import DataWarehouseEvent

AMQP_HEADERS = {'message-type': 'amqp-bridge'}
GL_HEADERS = {'message-type': 'gitlab'}
JIRA_HEADERS = {'message-type': 'jira'}
UMB_BRIDGE_HEADERS = {'message-type': 'cki.kwf.umb-bz-event'}
DATAWAREHOUSE_HEADERS = {'message-type': 'datawarehouse'}

MOCK_GL_USERNAME = 'botty'


def create_session(hook_name: str, args: str = '') -> SessionRunner:
    """Return a new SessionRunner with a fake gl_instance."""
    parsed_args = get_arg_parser('TEST').parse_args(args.split())
    session = SessionRunner.new(hook_name, parsed_args, {})
    mock_gl_instance = fakes.FakeGitLab()
    mock_gl_instance.auth()
    session.get_gl_instance = mock.Mock(return_value=mock_gl_instance)
    session.get_graphql = mock.Mock()
    return session


class TestGLEvent(KwfTestCase):
    """Tests for the GL Events."""

    def test_gl_merge_request_event_1(self) -> None:
        """Properties have the expected values."""
        test_session = create_session('bughook')
        body = self.load_yaml_asset('gl_merge_request_1.json', sub_module='webhook_events')
        test_event = session_events.create_event(test_session, GL_HEADERS, body)
        test_session.graphql.check_mr_state.return_value = StateTuple(
            commit_count=1,
            labels=test_event.event_labels,
            prepared_at=None,
            state=defs.MrState.OPENED
        )

        # GitlabEvent properties
        self.assertEqual(test_event.filters, session_events.GITLAB_MR_EVENT_FILTERS)
        self.assertEqual(test_event.triggers, session_events.GITLAB_MR_EVENT_TRIGGERS)
        self.assertIsInstance(test_event.created_at, datetime)
        self.assertEqual(test_event.draft_status, (False, False))
        self.assertEqual(test_event.user, 'cki-kwf-bot')
        self.assertIs(test_event.kind, session_events.GitlabObjectKind.MERGE_REQUEST)
        self.assertFalse(test_event.mr_inactive)
        self.assertEqual(test_event.merge_request, body['object_attributes'])
        self.assertEqual(len(test_event.event_labels), 20)
        self.assertTrue(
            all(isinstance(label, session_events.Label) for label in test_event.event_labels)
        )
        self.assertEqual(test_event.project, body['project'])
        self.assertEqual(test_event.namespace, 'redhat/centos-stream/src/kernel/centos-stream-9')
        self.assertEqual(test_event.mr_url, 'https://gitlab.com/redhat/centos-stream/'
                                            'src/kernel/centos-stream-9/-/merge_requests/2756')
        self.assertIsInstance(test_event.mr_url, session_events.GitlabURL)
        self.assertEqual(test_event.bot_users, {fakes.AUTH_USER['username']})

        # GitlabMREvent properties
        self.assertEqual(test_event.action, 'update')
        self.assertEqual(test_event.changes, body['changes'])
        self.assertFalse(test_event.closing)
        self.assertFalse(test_event.commits_changed)

        # BaseEvent properties
        self.assertIs(test_event.type, session_events.MessageType.GITLAB)
        self.assertEqual(test_event.rh_project.id, 24152864)
        self.assertTrue(test_event.matches_environment)
        self.assertTrue(test_event.filter_bots)
        self.assertTrue(test_event.passes_filters)
        self.assertFalse(test_event.matches_trigger)

    def test_gl_note_event_1(self) -> None:
        """Properties have the expected values."""
        test_session = create_session('ack_nack')
        body = self.load_yaml_asset('gl_note_1.json', sub_module='webhook_events')
        test_event = session_events.create_event(test_session, GL_HEADERS, body)

        # GitlabEvent properties
        self.assertEqual(test_event.filters, session_events.GITLAB_NOTE_EVENT_FILTERS)
        self.assertEqual(test_event.triggers, session_events.GITLAB_NOTE_EVENT_TRIGGERS)
        self.assertIsInstance(test_event.created_at, datetime)
        self.assertEqual(test_event.draft_status, (False, None))
        self.assertEqual(test_event.user, 'ptalbert')
        self.assertIs(test_event.kind, session_events.GitlabObjectKind.NOTE)
        self.assertFalse(test_event.mr_inactive)
        self.assertEqual(test_event.merge_request, body['merge_request'])
        self.assertEqual(len(test_event.event_labels), 20)
        self.assertTrue(
            all(isinstance(label, session_events.Label) for label in test_event.event_labels)
        )
        self.assertEqual(test_event.project, body['project'])
        self.assertEqual(test_event.namespace, 'cki-project/kernel-ark')
        self.assertEqual(test_event.mr_url, 'https://gitlab.com/cki-project/kernel-ark/'
                                            '-/merge_requests/2757')
        self.assertIsInstance(test_event.mr_url, session_events.GitlabURL)
        self.assertEqual(test_event.bot_users, {fakes.AUTH_USER['username']})

        # GitlabNoteEvent properties
        self.assertEqual(test_event.note_text, 'request-acks-evaluation')
        self.assertEqual(test_event.noteable_type, 'MergeRequest')
        self.assertEqual(test_event.match_note_text, [])

        # BaseEvent properties
        self.assertIs(test_event.type, session_events.MessageType.GITLAB)
        self.assertEqual(test_event.rh_project.id, 13604247)
        self.assertTrue(test_event.matches_environment)
        self.assertTrue(test_event.filter_bots)
        self.assertTrue(test_event.passes_filters)
        self.assertTrue(test_event.matches_trigger)

    def test_gl_note_block(self) -> None:
        """Matches a block action."""
        class BlockTest(typing.NamedTuple):
            note_text: str
            expected_match: str

        tests = [
            BlockTest("/block this MR, I don't like it", 'block'),
            BlockTest("/unblock this MR, I'm happy now", 'unblock'),
            BlockTest("I like cake", '')
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                test_session = create_session('ack_nack')
                body = self.load_yaml_asset('gl_note_1.json', sub_module='webhook_events')
                body['object_attributes']['note'] = test.note_text
                test_event = session_events.create_event(test_session, GL_HEADERS, body)
                action_matches = [match['action'] for match in test_event.match_note_text if
                                  'action' in match.groupdict()]
                if test.expected_match:
                    self.assertEqual(action_matches, [test.expected_match])
                else:
                    self.assertEqual(action_matches, [])

    def test_event_mr_is_open(self) -> None:
        """Returns True if the associated MR is currently open, or if no such MR is found."""
        class MrIsOpenTest(typing.NamedTuple):
            expected_result: bool
            mr_state: typing.Union[str, None] = None
            run_on_closed: bool = False
            run_on_closing: bool = False
            closing: typing.Union[bool, None] = None

        tests = [
            # No gl_mr so always returns True.
            MrIsOpenTest(
                expected_result=True,
                mr_state=None
            ),
            # A gl_mr but webhook has run_on_closed so skip the check.
            MrIsOpenTest(
                expected_result=True,
                mr_state='closed',
                run_on_closed=True
            ),
            # Has a gl_mr, is closing, but webhook does not run on closing, returns False.
            MrIsOpenTest(
                expected_result=False,
                mr_state='closed',
            ),
            # Has a gl_mr, is closing, webhook has run on closing, returns True.
            MrIsOpenTest(
                expected_result=True,
                mr_state='closed',
                run_on_closing=True,
                closing=True
            ),
            # Has a gl_mr and it is not closing and still open, returns True.
            MrIsOpenTest(
                expected_result=True,
                mr_state='opened',
            ),
            # Has a gl_mr and it is not closing and it is merged, returns False.
            MrIsOpenTest(
                expected_result=False,
                mr_state='merged',
            ),
        ]

        # This event type won't naturally have a 'closing' attribute.
        body = {'object_kind': 'pipeline'}

        for test in tests:
            with self.subTest(**test._asdict()):
                test_session = create_session('ack_nack')
                test_session.webhook.run_on_closed = test.run_on_closed
                test_session.webhook.run_on_closing = test.run_on_closing
                test_event = session_events.create_event(test_session, GL_HEADERS, body)
                if test.mr_state:
                    test_event.mr_state_tuple = \
                        StateTuple(
                            commit_count=0,
                            labels=[],
                            prepared_at=None,
                            state=defs.MrState.from_str(test.mr_state))
                else:
                    test_event.mr_state_tuple = None
                if test.closing is not None:
                    test_event.closing = test.closing
                self.assertEqual(test_event.mr_is_open, test.expected_result)

    def test_event_mr_is_oversize(self) -> None:
        """Returns True if the associated MR has too many commits, otherwise False."""
        class MrIsOversizeTest(typing.NamedTuple):
            expected_result: bool
            commit_count: typing.Union[int, None]
            run_on_oversize_mrs: bool = False

        tests = [
            # No mr_state_tuple so always return False.
            MrIsOversizeTest(
                expected_result=False,
                commit_count=None
            ),
            # Webhook.run_on_oversize_mrs is set, returns False no matter what.
            MrIsOversizeTest(
                expected_result=False,
                commit_count=999999,
                run_on_oversize_mrs=True
            ),
            # A big one, returns True
            MrIsOversizeTest(
                expected_result=True,
                commit_count=10000,
            ),
            # A perfectly fine MR.
            MrIsOversizeTest(
                expected_result=False,
                commit_count=15
            ),
        ]

        # This event type won't naturally have a 'closing' attribute.
        body = {'object_kind': 'merge_request'}

        for test in tests:
            with self.subTest(**test._asdict()):
                test_session = create_session('ack_nack')
                test_session.webhook.run_on_oversize_mrs = test.run_on_oversize_mrs
                test_event = session_events.create_event(test_session, GL_HEADERS, body)
                if test.commit_count is not None:
                    test_event.mr_state_tuple = \
                        StateTuple(
                            commit_count=test.commit_count,
                            labels=[],
                            prepared_at=None,
                            state=defs.MrState.UNKNOWN)
                else:
                    test_event.mr_state_tuple = None
                self.assertEqual(test_event.mr_is_oversize, test.expected_result)


class TestGitlabEventFilters(KwfTestCase):
    """Test cases for the GitlabEvent Filters."""
    # Filter functions return True when an Event passes the filter.
    # Filter functions return False when an Event fails the filter and should be ignored.

    def test_filter_drafts(self):
        """Returns False if the MR is draft and the event does not indicate that just changed."""
        class DraftsTest(typing.NamedTuple):
            passes_filter: bool
            event_body_file: str = 'gl_merge_request_1.json'
            webhook_run_on_drafts: bool = False
            body_mr_is_draft: typing.Union[bool, None] = None
            changed_previous_current: typing.Union[typing.Tuple[bool, bool], None] = None

        tests = [
            # Webhook.run_on_drafts=True, passes filter automatically.
            DraftsTest(passes_filter=True, webhook_run_on_drafts=True),
            # Webhook.run_on_drafts=False, MR is not draft and has not changes, passes.
            DraftsTest(passes_filter=True, body_mr_is_draft=False),
            # MR is draft and has not changed, fails.
            DraftsTest(passes_filter=False, body_mr_is_draft=True),
            # MR is draft but just changed, passes.
            DraftsTest(
                passes_filter=True,
                body_mr_is_draft=True,
                changed_previous_current=(False, True)
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                mock_session = mock.Mock(spec_set=['webhook'])
                mock_session.webhook.run_on_drafts = test.webhook_run_on_drafts
                body = self.load_yaml_asset(test.event_body_file, sub_module='webhook_events')
                if test.body_mr_is_draft is not None:
                    mr_key = 'object_attributes' if body['object_kind'] == 'merge_request' else \
                        'merge_request'
                    body[mr_key]['draft'] = test.body_mr_is_draft
                if test.changed_previous_current:
                    body['changes']['draft'] = {'previous': test.changed_previous_current[0],
                                                'current': test.changed_previous_current[1]}
                else:
                    body.get('changes', {}).pop('draft', None)
                test_event = session_events.create_event(mock_session, GL_HEADERS, body)
                self.assertEqual(test.passes_filter, test_event.filter_drafts)


class TestDataWarehouseEvent(KwfTestCase):
    """Test properties and filters from DataWarehouseEvent."""

    def setUp(self):
        """Prepare dw_event instance."""
        self.mocked_session = mock.Mock()
        self.dw_event_body = deepcopy(fake_payloads.DATAWAREHOUSE_PAYLOAD)
        self.dw_event: "DataWarehouseEvent" = session_events.create_event(
            self.mocked_session, DATAWAREHOUSE_HEADERS, body=self.dw_event_body)

    def test_property_status(self):
        """Test DataWarehouseEvent.status works as expected."""
        result = self.dw_event.status
        expected_status = "checkout_issueoccurrences_changed"
        self.assertEqual(expected_status, result)

    def test_property_object_type(self):
        """Test DataWarehouseEvent.object_type works as expected."""
        result = self.dw_event.object_type
        expected_object_type = "checkout"
        self.assertEqual(expected_object_type, result)

    def test_property_object(self):
        """Test DataWarehouseEvent.object works as expected."""
        result = self.dw_event.object
        expected_object = {"id": "redhat:1", "origin": "redhat", "misc": {"iid": 1, "provenance": [
            {"service_name": "beaker", "url": "https://beaker-project.com/recipes/14448356"},
            {"service_name": "gitlab",
             "url": "https://gitlab.com/redhat/linux/-/pipelines/1234567890"},
        ]}}
        self.assertEqual(expected_object, result)

    def test_property_misc(self):
        """Test DataWarehouseEvent.misc works as expected."""
        result = self.dw_event.misc
        expected_misc = {"extra": "ketchup"}
        self.assertEqual(expected_misc, result)

    def test_property_filter_status(self):
        """Test DataWarehouseEvent.filter_status works as expected."""
        with self.subTest("Base fixture should pass the filter", status=self.dw_event.status):
            self.assertTrue(self.dw_event.filter_status)

        unsupported_status = "new"
        with self.subTest("The same fixture with an unsupported status should not pass the filter",
                          unsupported_status=unsupported_status):
            new_body = deepcopy(self.dw_event_body)
            new_body["status"] = unsupported_status
            new_dw_event: "DataWarehouseEvent" = session_events.create_event(
                self.mocked_session, DATAWAREHOUSE_HEADERS, body=new_body)
            self.assertFalse(new_dw_event.filter_status,
                             f"Expects {unsupported_status=} to be filtered out")

    def test_property_filter_object_type(self):
        """Test DataWarehouseEvent.filter_object_type works as expected."""
        with self.subTest("Base fixture should pass the filter", status=self.dw_event.object_type):
            self.assertTrue(self.dw_event.filter_object_type)

        unsupported_object_type = "test"
        with self.subTest(
                "The same fixture with an unsupported object_type should not pass the filter",
                unsupported_object_type=unsupported_object_type):
            new_body = deepcopy(self.dw_event_body)
            new_body["object_type"] = unsupported_object_type
            new_dw_event: "DataWarehouseEvent" = session_events.create_event(
                self.mocked_session, DATAWAREHOUSE_HEADERS, body=new_body)
            self.assertFalse(new_dw_event.filter_object_type,
                             f"Expects {unsupported_object_type=} to be filtered out")
