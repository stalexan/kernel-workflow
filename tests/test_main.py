"""Tests for the common main() function bits."""
import argparse
import importlib
import os
import typing
from unittest import mock

from tests.helpers import KwfTestCase


class MainTest(typing.NamedTuple):
    """Parameters for TestMain.hook_main_test."""

    hook_name: str
    args: str = ''
    env: typing.Optional[typing.Dict[str, str]] = None
    extra_arg_names: typing.Optional[typing.List[str]] = None
    clean_exit: bool = True
    raises: bool = False


class TestMain(KwfTestCase):
    """Tests for the common main() bits."""

    def hook_main_test(self, test: MainTest) -> None:
        """Test a hook's main() function by validating it has the expected args."""
        hook_name, args, environment, extra_arg_names, clean_exit, raises = test
        args = args.split() if args else []
        hook_module = importlib.import_module(f'webhook.{hook_name}')

        with mock.patch(f'webhook.{hook_name}.SessionRunner') as mock_session_runner:
            with mock.patch.dict(os.environ, environment or {}, clear=True):
                with mock.patch('sys.exit') as mock_exit:
                    # Do we expect an exception to be raised?
                    if raises:
                        with self.assertRaises(Exception):
                            hook_module.main(args)
                        return
                    hook_module.main(args)

                    # Did we expect parse_args() to call sys.exit()?
                    if not clean_exit:
                        mock_exit.assert_called_once()
                        return
                    mock_exit.assert_not_called()

            mock_session_runner.new.assert_called_once_with(
                hook_name,
                args=mock.ANY,
                handlers=mock.ANY
            )
            # Get the mock SessionRunner instance and argparse.Namespace.
            mock_session = mock_session_runner.new.return_value
            args_namespace = mock_session_runner.new.call_args.kwargs['args']

        # Confirm args_namespace really is an argparse.Namespace.
        self.assertIsInstance(args_namespace, argparse.Namespace)

        # Confirm all the additional arguments are present in the argparse.Namespace.
        for arg_name in extra_arg_names or []:
            self.assertTrue(
                hasattr(args_namespace, arg_name.strip('--').replace('-', '_')),
                msg=f"Expected args Namespace to have attribute for '{arg_name}'")
        # Confirm the SessionRunner.run() was called.
        mock_session.run.assert_called_once()

    def test_main_funtions(self) -> None:
        """Test each hook main() function."""
        tests = [
            MainTest(
                hook_name='ack_nack',
                extra_arg_names=['--rhkernel-src'],
                clean_exit=False
            ),
            MainTest(
                hook_name='ack_nack',
                env={'RHKERNEL_SRC': '/path'},
                extra_arg_names=['--rhkernel-src'],
            ),
            MainTest(
                hook_name='bughook',
            ),
            MainTest(
                hook_name='buglinker',
            ),
            MainTest(
                hook_name='ckihook',
            ),
            MainTest(
                hook_name='commit_compare',
                args='--linux-src /var/mnt/data/git/linux',
                extra_arg_names=['--linux-src']
            ),
            MainTest(
                hook_name='fixes',
                env={'LINUX_SRC': '/path/linux'},
                extra_arg_names=['--linux-src']
            ),
            MainTest(
                hook_name='jirahook',
            ),
            MainTest(
                hook_name='mergehook',
                env={'RHKERNEL_SRC': '/home/torvalds/linus'},
                extra_arg_names=['--rhkernel-src']
            ),
            MainTest(
                hook_name='signoff',
            ),
            MainTest(
                hook_name='sprinter',
            ),
            MainTest(
                hook_name='subsystems',
                extra_arg_names=['--local-repo-path', '--linus-src'],
                clean_exit=False,
                raises=True
            ),
            MainTest(
                hook_name='subsystems',
                args='--local-repo-path /checkouts/kernel-watch',
                env={'LINUS_SRC': r'c:\Program Files\linux\src'},
                extra_arg_names=['--local-repo-path', '--linus-src'],
                raises=True
            ),
            MainTest(
                hook_name='subsystems',
                args='--local-repo-path /checkouts/kernel-watch',
                env={
                    'LINUS_SRC': r'c:\Program Files\linux\src',
                    'KERNEL_WATCH_URL': 'https://kernel-watch.biz'
                },
                extra_arg_names=['--local-repo-path', '--linus-src'],
            ),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                self.hook_main_test(test)
