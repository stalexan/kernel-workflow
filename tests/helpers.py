"""Helper bits for kernel-workflow unittests."""
from importlib import resources
import typing
from unittest import TestCase
from unittest import mock

from cki_lib.yaml import load as cki_yaml_load
import freezegun
import jira
import responses

from tests import fake_payloads
from webhook import session


class KwfTestCase(TestCase):
    """TestCase with socket.socket disabled."""

    def setUp(self):
        """Set up each test with socket.socket patched."""
        super().setUp()
        # Set up some defs.
        self.GITLAB_API = 'https://gitlab.com/api/v4'
        self.GITLAB_GRAPHQL = fake_payloads.GITLAB_GRAPHQL
        # Patch socket.socket.
        patched_socket = mock.patch('socket.socket', mock.Mock())
        patched_socket.start()
        self.addCleanup(patched_socket.stop)
        # Set up responses.
        self.responses = responses.RequestsMock()
        self.responses.start()
        self.addCleanup(self.responses.stop)
        self.addCleanup(self.responses.reset)
        # Clear the webhook.session caches.
        self.clear_caches()

    @staticmethod
    def clear_caches() -> None:
        """Clear the webhook.session caches."""
        session.get_gl_group.cache_clear()
        session.get_gl_instance.cache_clear()
        session.get_gl_project.cache_clear()
        session.get_graphql.cache_clear()

    @staticmethod
    def load_yaml_asset(
        path: str,
        module: str = 'tests.assets',
        sub_module: typing.Optional[str] = None
    ) -> dict | list:
        """Return the yaml (or json) test asset contents from the given module.sub_module."""
        if sub_module:
            module += f'.{sub_module}'
        return cki_yaml_load(file_path=resources.files(module).joinpath(path))

    @classmethod
    def make_jira_issue(cls, key: str) -> jira.resources.Issue:
        """Return a jira.Issue loaded with the test asset json matching the given key."""
        raw_issue = cls.load_yaml_asset(f'{key}.json', sub_module='jira_rest_api')
        return jira.resources.Issue(options={}, session={}, raw=raw_issue)

    def base_session(self) -> session.BaseSession:
        """Return a BaseSession object."""
        return session.BaseSession.new()

    def response_gql_user_data(self, **kwargs) -> responses.Response:
        """Return a responses.Response for graphql.GitlabGraph.user."""
        if 'rsps' not in kwargs:
            kwargs['rsps'] = self.responses
        return fake_payloads.mock_gql_user_data(**kwargs)

    def response_gl_auth(
        self,
        username: str = fake_payloads.USER_NAME,
        id: int = fake_payloads.USER_ID,
        body: typing.Optional[typing.Any] = None
    ) -> responses.Response:
        """Return a responses.Response for gitlab.auth()."""
        if body:
            return self.responses.get(f'{self.GITLAB_API}/user', body=body)
        return self.responses.get(f'{self.GITLAB_API}/user', json={'id': id, 'username': username})

    def freeze_time(
        self,
        time_str: str,
        start: bool = True,
        cleanup: bool = True
    ) -> freezegun.api._freeze_time:
        """Create a freeze time object and start it."""
        freezer = freezegun.freeze_time(time_str)
        if cleanup:
            self.addCleanup(freezer.stop)
        if start:
            freezer.start()
        return freezer
