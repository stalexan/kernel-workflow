"""Tests for the table module."""

from tests import fakes_jira
from tests import fakes_jira_mrs as fakes_mrs
from tests.helpers import KwfTestCase
from webhook import defs
from webhook import rhissue
from webhook import rhissue_tests
from webhook.rh_metadata import Projects


class TestRHIssue(KwfTestCase):
    """Tests for the RHIssue class."""

    # expected Issue values when no JIRA or MRs are set.
    empty_mr_equal = {'_mrs': [],
                      'alias': 'RHIssue #0',
                      'failed_tests': [],
                      '_id': 0,
                      'id': 'BOGUS',       # This should be set by the tester,
                      'ji_cves': [],
                      'ji_depends_on': [],
                      'ji_fix_version': None,
                      'labels': [],
                      'policy_check_ok': (None, 'Check not done: No JIRA Issue'),
                      'commits': [],
                      'parent_mr_commits': []}

    empty_mr_is = {'_ji': None,
                   'scope': defs.MrScope.INVALID,
                   'internal': False,
                   'untagged': False,
                   'ji': None,
                   'mr': None,
                   'parent_mr': None,
                   'ji_branch': None,
                   'ji_pt_status': defs.JIPTStatus.UNKNOWN,
                   'ji_project': None,
                   'ji_resolution': None,
                   'ji_status': defs.JIStatus.UNKNOWN,
                   'cve_ids': None,
                   'is_cve_tracker': False,
                   'is_dependency': False,
                   'is_linked_issue': False,
                   'is_merged': False,
                   'in_mr_description': False,
                   'test_list': ['BOGUS']}  # This should be set by the tester.

    def validate_issue(self, rhissue, assert_equal, assert_is):
        """Helper to validate a JIRA Issue object."""
        print(f'Testing RHIssue {rhissue}...')
        for attribute, value in assert_equal.items():
            print(f'{attribute} should equal: {value}')
            self.assertCountEqual(getattr(rhissue, attribute), value) if \
                isinstance(value, list) else self.assertEqual(getattr(rhissue, attribute), value)
        for attribute, value in assert_is.items():
            print(f'{attribute} should be: {value}')
            self.assertIs(getattr(rhissue, attribute), value)

    def test_rhissue_init_empty(self):
        """Creates a RHIssue with no JIRA object set or MRs."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Expected values for an empty RHIssue.
        assert_equal['id'] = 0
        assert_equal['_id'] = 0
        assert_is.pop('_id', None)
        assert_is['test_list'] = rhissue_tests.ISSUE_TESTS

        test_rhissue = rhissue.RHIssue()
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_internal_no_mrs(self):
        """Creates a RHIssue representing descriptions marked INTERNAL."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Unique values of an Internal JIRA Issue
        assert_equal['alias'] = 'RHIssue #INTERNAL'
        assert_equal['id'] = rhissue.INTERNAL_JISSUE
        assert_is['internal'] = True
        assert_is['test_list'] = rhissue_tests.INTERNAL_TESTS

        test_rhissue = rhissue.RHIssue.new_internal(mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_missing_no_mrs(self):
        """Creates a RHIssue representing a MISSING JI."""
        issue_id = f'{defs.JPFX}1234567'
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Expected values for this Missing RHIssue
        assert_equal['alias'] = f'RHIssue #{defs.JPFX}1234567'
        assert_equal['id'] = issue_id
        assert_equal['_id'] = issue_id
        assert_is.pop('_id', None)
        assert_is['test_list'] = rhissue_tests.ISSUE_TESTS

        test_rhissue = rhissue.RHIssue.new_missing(issue_id, mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_missing_cve_no_mrs(self):
        """Creates a RHIssue representing a MISSING CVE tracker JI."""
        issue_id = 'CVE-1998-12345'
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Expected values for this Missing CVE RHIssue
        assert_equal['alias'] = 'CVE-1998-12345 (JIRA Issue missing)'
        assert_equal['policy_check_ok'] = (None, 'Check not done: CVE tracker issue')
        assert_equal['cve_ids'] = [issue_id]
        assert_equal['id'] = issue_id
        assert_equal['_id'] = issue_id
        assert_is.pop('_id', None)
        assert_is.pop('cve_ids', None)
        assert_is['is_cve_tracker'] = True
        assert_is['test_list'] = rhissue_tests.CVE_TESTS

        test_rhissue = rhissue.RHIssue.new_missing(issue_id, mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_untagged_no_mrs(self):
        """Creates a RHIssue representing descriptions with no tags (UNTAGGED)."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Unique values of an Untagged RHIssue
        assert_equal['alias'] = 'RHIssue #UNTAGGED'
        assert_equal['id'] = rhissue.UNTAGGED_JISSUE
        assert_is['untagged'] = True
        assert_is['test_list'] = rhissue_tests.UNTAGGED_TESTS

        test_rhissue = rhissue.RHIssue.new_untagged(mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_from_ji_no_mrs_ji1234567(self):
        """Creates a RHIssue linked to the given JIRA Issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'RHIssue #{defs.JPFX}1234567'
        assert_equal['id'] = f'{defs.JPFX}1234567'
        assert_equal['ji_cves'] = ['CVE-1235-13516']
        assert_equal['labels'] = ['CVE-1235-13516', 'security']
        assert_equal['ji_fix_version'] = 'rhel-9.1.0'
        assert_equal['policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['ji'] = fakes_jira.JI1234567
        assert_is['_ji'] = fakes_jira.JI1234567
        assert_is['ji_status'] = defs.JIStatus.IN_PROGRESS
        assert_is['test_list'] = rhissue_tests.ISSUE_TESTS

        test_rhissue = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI1234567, mrs=[])
        print(f'ji fields labels is {test_rhissue.ji.fields.labels}')
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_from_ji_no_mrs_ji2323232(self):
        """Creates a RHIssue linked to the given JIRA Issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'RHIssue #{defs.JPFX}2323232'
        assert_equal['id'] = f'{defs.JPFX}2323232'
        assert_equal['ji_fix_version'] = 'rhel-9.1.0'
        assert_equal['policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['ji'] = fakes_jira.JI2323232
        assert_is['_ji'] = fakes_jira.JI2323232
        assert_is['ji_status'] = defs.JIStatus.READY_FOR_QA
        assert_is['test_list'] = rhissue_tests.ISSUE_TESTS
        assert_is['ji_pt_status'] = defs.JIPTStatus.PASS

        test_rhissue = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI2323232, mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_from_ji_no_mrs_ji7777777(self):
        """Creates a RHIssue linked to the given JIRA issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'RHIssue #{defs.JPFX}7777777'
        assert_equal['id'] = f'{defs.JPFX}7777777'
        assert_equal['ji_fix_version'] = 'rhel-9.1'
        assert_equal['policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['ji'] = fakes_jira.JI7777777
        assert_is['_ji'] = fakes_jira.JI7777777
        assert_is['ji_status'] = defs.JIStatus.IN_PROGRESS
        assert_is['test_list'] = rhissue_tests.ISSUE_TESTS

        test_rhissue = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI7777777, mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_from_ji_no_mrs_ji2345678(self):
        """Creates a RHIssue linked to the given JIRA Issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'RHIssue #{defs.JPFX}2345678'
        assert_equal['id'] = f'{defs.JPFX}2345678'
        assert_equal['ji_cves'] = ['CVE-2022-43210']
        assert_equal['labels'] = ['boop', 'CVE-2022-43210']
        assert_equal['ji_fix_version'] = 'rhel-9.1.0'
        assert_equal['policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['ji'] = fakes_jira.JI2345678
        assert_is['_ji'] = fakes_jira.JI2345678
        assert_is['ji_status'] = defs.JIStatus.READY_FOR_QA
        assert_is['test_list'] = rhissue_tests.ISSUE_TESTS
        assert_is['ji_pt_status'] = defs.JIPTStatus.PASS

        test_rhissue = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI2345678, mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_from_ji_no_mrs_ji3456789(self):
        """Creates a RHIssue linked to the given JIRA Issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'CVE-1235-13516 (RHIssue #{defs.JPFX}3456789)'
        assert_equal['id'] = f'{defs.JPFX}3456789'
        assert_equal['ji_cves'] = ['CVE-1235-13516']
        assert_equal['policy_check_ok'] = (None, 'Check not done: CVE tracker issue')
        assert_equal['cve_ids'] = ['CVE-1235-13516']
        assert_equal['labels'] = ['help', 'CVE-1235-13516']
        assert_is.pop('cve_ids', None)
        assert_is['ji'] = fakes_jira.JI3456789
        assert_is['_ji'] = fakes_jira.JI3456789
        assert_is['ji_status'] = defs.JIStatus.NEW
        assert_is['is_cve_tracker'] = True
        assert_is['test_list'] = rhissue_tests.CVE_TESTS
        assert_is['ji_pt_status'] = defs.JIPTStatus.UNSET

        test_rhissue = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI3456789, mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_rhissue_new_from_ji_no_mrs_ji4567890(self):
        """Creates a RHIssue linked to the given JIRA Issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'CVE-2022-7549, CVE-2022-7550 (RHIssue #{defs.JPFX}4567890)'
        assert_equal['id'] = f'{defs.JPFX}4567890'
        assert_equal['ji_cves'] = ['CVE-2022-7549', 'CVE-2022-7550']
        assert_equal['policy_check_ok'] = (None, 'Check not done: CVE tracker issue')
        assert_equal['cve_ids'] = ['CVE-2022-7549', 'CVE-2022-7550']
        assert_equal['labels'] = ['CVE-2022-7549', 'CVE-2022-7550', 'bing', 'bong']
        assert_is.pop('cve_ids', None)
        assert_is['ji'] = fakes_jira.JI4567890
        assert_is['_ji'] = fakes_jira.JI4567890
        assert_is['ji_resolution'] = defs.JIResolution.ERRATA
        assert_is['ji_status'] = defs.JIStatus.CLOSED
        assert_is['is_cve_tracker'] = True
        assert_is['test_list'] = rhissue_tests.CVE_TESTS
        assert_is['ji_pt_status'] = defs.JIPTStatus.UNSET

        test_rhissue = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI4567890, mrs=[])
        self.validate_issue(test_rhissue, assert_equal, assert_is)

    def test_equality(self):
        """Returns True if the IDs match, otherwise False."""
        rhissue1 = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI3456789, mrs=[])
        rhissue2 = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI4567890, mrs=[])
        rhissue3 = rhissue.RHIssue.new_missing(ji_id=f'{defs.JPFX}3456789', mrs=[])

        self.assertNotEqual(rhissue1, rhissue2)
        self.assertEqual(rhissue1, rhissue3)

        # Returns False if the other is not a RHIssue instance.
        self.assertFalse(rhissue1 == 'hello')

    def test_id_setter(self):
        """Sets the id property if self.ji is not set and not internal/untagged."""
        # Setting id property raises ValueError if the ji property is set.
        test_rhissue = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI2323232, mrs=[])
        with self.assertRaises(ValueError):
            test_rhissue.id = f'{defs.JPFX}1234567'

        # Setting the id property of a RHIssue marked internal or untagged raises a ValueError
        test_rhissue = rhissue.RHIssue.new_internal(mrs=[])
        with self.assertRaises(ValueError):
            test_rhissue.id = f'{defs.JPFX}7654321'
        test_rhissue = rhissue.RHIssue.new_untagged(mrs=[])
        with self.assertRaises(ValueError):
            test_rhissue.id = f'{defs.JPFX}7654321'

        # Setting the id property works for a RHIssue with no ji.
        test_rhissue = rhissue.RHIssue.new_missing(f'{defs.JPFX}1234567', mrs=[])
        self.assertEqual(test_rhissue.id, f'{defs.JPFX}1234567')
        test_rhissue.id = f'{defs.JPFX}7654321'
        self.assertEqual(test_rhissue.id, f'{defs.JPFX}7654321')

    def test_rhissues_with_mrs(self):
        """Returns the expected values when MRs are present."""
        projects = Projects(extra_projects_paths=['tests/assets/rh_projects_private.yaml'])
        mr410 = fakes_mrs.make_mr('redhat/centos-stream/src/kernel/centos-stream-9', 410,
                                  projects=projects,
                                  query_results_list=[fakes_mrs.MR410_DICT, fakes_mrs.MR404_DICT])
        mr404 = fakes_mrs.make_mr('redhat/centos-stream/src/kernel/centos-stream-9', 404,
                                  projects=projects, query_results_list=[fakes_mrs.MR404_DICT],
                                  is_dependency=True)
        mr410.depends_mrs.append(mr404)
        mr410.description.depends_bzs.update({f'{defs.JPFX}2323232'})
        mrs = [mr410, mr404]
        ji1234567 = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI1234567, mrs=mrs)
        ji2323232 = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI2323232, mrs=mrs)
        ji3456789 = rhissue.RHIssue.new_from_ji(ji=fakes_jira.JI3456789, mrs=mrs)

        # ji1234567 properties
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'RHIssue #{defs.JPFX}1234567'
        assert_equal['id'] = f'{defs.JPFX}1234567'
        assert_equal['ji_cves'] = ['CVE-1235-13516']
        assert_equal['labels'] = ['CVE-1235-13516', 'security']
        assert_equal['ji_fix_version'] = 'rhel-9.1.0'
        assert_equal['policy_check_ok'] = (None, 'Check not done: Branch has no policy')
        assert_equal['commits'] = ['0aa467549b4e997d023c29f4d481aee01b9e9471',
                                   'e53eab9f887f784044ad32ef5c082695831d90d9',
                                   '88cdd4035228dac16878eb907381afea6ceffeaa']
        assert_equal['parent_mr_commits'] = assert_equal['commits']
        assert_equal['_mrs'] = mrs
        assert_is['ji'] = fakes_jira.JI1234567
        assert_is['_ji'] = fakes_jira.JI1234567
        assert_is['ji_branch'] = mr410.projects.projects[24152864].branches[0]
        assert_is['ji_project'] = mr410.projects.projects[24152864]
        assert_is['ji_status'] = defs.JIStatus.IN_PROGRESS
        assert_is['in_mr_description'] = True
        assert_is['mr'] = mr410
        assert_is['parent_mr'] = mr410
        assert_is['test_list'] = rhissue_tests.ISSUE_TESTS

        self.validate_issue(ji1234567, assert_equal, assert_is)

        # ji2323232 properties
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'RHIssue #{defs.JPFX}2323232'
        assert_equal['id'] = f'{defs.JPFX}2323232'
        assert_equal['policy_check_ok'] = (None, 'Check not done: Branch has no policy')
        assert_equal['ji_fix_version'] = 'rhel-9.1.0'
        assert_equal['commits'] = ['ce1fdd9354bdc315e49a40dc9da3ab03bf6af7b3',
                                   'f77278fcd9cef99358adc7f5e077be795a54ffca']
        assert_equal['parent_mr_commits'] = assert_equal['commits']
        assert_equal['_mrs'] = mrs
        assert_is['ji'] = fakes_jira.JI2323232
        assert_is['_ji'] = fakes_jira.JI2323232
        assert_is['ji_branch'] = mr410.projects.projects[24152864].branches[0]
        assert_is['ji_project'] = mr410.projects.projects[24152864]
        assert_is['ji_status'] = defs.JIStatus.READY_FOR_QA
        assert_is['in_mr_description'] = True
        assert_is['is_dependency'] = True
        assert_is['is_linked_issue'] = False
        assert_is['mr'] = mr404
        assert_is['parent_mr'] = mr410
        assert_is['test_list'] = rhissue_tests.DEP_TESTS
        assert_is['ji_pt_status'] = defs.JIPTStatus.PASS

        self.validate_issue(ji2323232, assert_equal, assert_is)

        # ji3456789 properties
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'CVE-1235-13516 (RHIssue #{defs.JPFX}3456789)'
        assert_equal['id'] = f'{defs.JPFX}3456789'
        assert_equal['ji_cves'] = ['CVE-1235-13516']
        assert_equal['labels'] = ['help', 'CVE-1235-13516']
        assert_equal['policy_check_ok'] = (None, 'Check not done: CVE tracker issue')
        assert_equal['cve_ids'] = ['CVE-1235-13516']
        assert_equal['commits'] = ['0aa467549b4e997d023c29f4d481aee01b9e9471',
                                   'e53eab9f887f784044ad32ef5c082695831d90d9',
                                   '88cdd4035228dac16878eb907381afea6ceffeaa']
        assert_equal['parent_mr_commits'] = assert_equal['commits']
        assert_equal['_mrs'] = mrs
        assert_is.pop('cve_ids', None)
        assert_is['ji'] = fakes_jira.JI3456789
        assert_is['_ji'] = fakes_jira.JI3456789
        assert_is['ji_status'] = defs.JIStatus.NEW
        assert_is['in_mr_description'] = True
        assert_is['is_cve_tracker'] = True
        assert_is['mr'] = mr410
        assert_is['parent_mr'] = mr410
        assert_is['test_list'] = rhissue_tests.CVE_TESTS
        assert_is['ji_pt_status'] = defs.JIPTStatus.UNSET

        self.validate_issue(ji3456789, assert_equal, assert_is)

        mr410.depends_mrs.clear()
